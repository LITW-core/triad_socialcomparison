
/*************************************************************
 * pages.js
 * 
 * Contains all the functions for showing the different pages
 * of the "analytic-holistic" experiment
 * 
 * Author: Yuechen Zhao
 * Last Modified: January, 2015
 * Modified by: Trevor Croxson
 * 
 * © Copyright 2016 LabintheWild.
 * For questions about this file and permission to use
 * the code, contact us at info@labinthewild.org
 *************************************************************/

  <?php 
 	include_once("../localization.php");
 ?>

// demographics page
function showDemographics() {
	showSlide("demographics");
	showNextButton(submit);
	$("#header").show();
	demographics();
}

function insertHeader() {
	$("#header").html("<a href='http://www.labinthewild.org'><img src='../common/img/logo.png' alt='logo' id='logo'/></a><span id='share_text'>" + 
		<?php echo _("'Tell your friends about this test!'"); ?> + "</span>");
	$("#header_share").detach().appendTo("#header");
	$("#header_share").show();
	$("#header").append("<div class='separator'> &nbsp; </div>");
}

// show the study info page
function studyInfo() {
	insertHeader();
	$$$("studyinfo").innerHTML = 
		"<h2 class='bolded-blue'>" + "<?php echo _('Test your Thinking Style'); ?>" + "</h2>" +
		"<p>" + "<?php echo _('You\'re about to take a test on LabintheWild. Your participation allows us to learn more about human analytic abilities around the world. The results from your test will also tell you something about yourself!'); ?>" + "</p>" + 
		"<p><em>" + "<?php echo _('Please read the following information carefully before proceeding.'); ?>" + "</em></p>" + 
		"<p>" + "<?php echo _('<strong>Why we are doing this research:</strong> We are trying to understand how our cultural backgrounds affect our thinking styles. This research will help us to develop strategies and tools for designing culturally-sensitive websites.'); ?>" + "</p>" +
		"<p>" + "<?php echo _('<strong>What you will have to do:</strong> You will complete two different tasks. In one task, you will be shown three words. Using your mouse, you will need to click on the two words that you feel go together best. In the other task, you will need to use your mouse to select a group of images that best matches a target image. Further instructions about this task will follow.'); ?>" + "</p>" +
		"<p>" + "<?php echo _('<strong>What you will get out of it:</strong> We will give you feedback on how your results compare to those of other participants. The final results from this experiment will be posted on our blog page. The experiment is not designed to benefit you, but you may enjoy it and enjoy comparing your results with those of other participants.'); ?>" + "</p>" +
		"<p>" + "<?php echo _('<strong>Privacy and Data Collection:</strong> We will not ask you for your name. Any data that we collect will be securely stored on our servers.'); ?>" + "</p>" +
		"<p>" + "<?php echo _('<strong>Duration:</strong> Approximately 5 minutes.'); ?>" + "</p>" +
		"<p>" + "<?php echo _('<strong>Contact information:</strong> If you have questions or concerns about this research, you may contact Professor Katharina Reinecke, Paul G. Allen Center for Computer Science & Engineering, Box 352350, Seattle, WA 98195, '); ?>" + "<a href='mailto:reinecke@cs.washington.edu'>reinecke@cs.washington.edu</a>." + 
		"<p>" + "<?php echo _('If you have questions about your rights as a research participant, or wish to obtain information, ask questions or discuss any concerns about this study with someone other than the researcher(s), please contact the University of Washington Human Subjects Division at 206-543-0098 (for international calls include the US Calling Code: +1-206-543-0098).'); ?>" +
		"<form><input type='checkbox' id='agreeToStudy'/>" +
		"<label for='agreeToStudy'>" + "<?php echo _(' By ticking this box, you are agreeing to be in the study. Be sure that questions you have about the study have been answered and that you understand what you are being asked to do. You may contact the researcher if you think of a question later. You are free to leave the experiment at any time, and refusing to be in the experiment or stopping participation will involve no penalty or loss of benefits to which you are otherwise entitled.'); ?>" + 
		"</label></form>";

	$("#header").show();
	showSlide("studyinfo");
	$$$("next_button").innerHTML = "<span class='bolded-blue'><br/>" + "<?php echo _('You must check the box to continue.'); ?>" + "</span>";
	$$$("agreeToStudy").onclick = 
		function() { 
			if ($$$("agreeToStudy").checked) {
				showNextButton(nextTrial);
			} else {
			//	disableKeyboard();
				$$$("next_button").innerHTML = "<span class='bolded-blue'><br/>" + "<?php echo _('You must check the box to continue.'); ?>" + "</span>";
			}
		}
}

// show the fail page
function showFailPage() {
	$$$("content").style.display = "none";
	window.onbeforeunload = null;
	$$$("error").style.display = "block";
	throw new Error('Fatal Error');
}

var code = Math.random();

// show the comments page
function showCommentsPage() {
	$("#trialNum").remove();
	$("#ajax_working").css("visibility", "hidden");
	$("#header").show();
	if (withTouch)
		enableElasticScrolling();
	
	$$$("comments").innerHTML = 
		"<h2 class='bolded-blue'>" + "<?php echo _('Thank you for your participation!'); ?>" + "</h2>" +
		"<p><br/>" + "<?php echo _('Before you continue to your results, please let us know what you thought of the test!'); ?>" +
		"<br/><em>" + "<?php echo _('If you require our response, please include your email.'); ?>" + "</em><br/><br/></p>" +
		"<form id='comments_form'>" +
		"<p>"+ "<?php echo _('Do you have any comments for the researcher? Questions, Suggestions, or Concerns?'); ?>" + "</p>" +
		"<textarea class='comments_box' id='general' name='general' onfocus='onFocusBox(this);' onblur='onBlurBox(this);'>" + 
		"<?php echo _('You may leave this area blank.'); ?>" + "</textarea>" +
		"<br/>" +
		"<p>" + "<?php echo _('Did you encounter any technical difficulties during this study? If yes, how?'); ?>" + " &nbsp; " +
		"<input type='radio' name='tech' id='tech_yes' value='yes' /> <label for='tech_yes'>" + "<?php echo _('Yes') ?>" + "</label> " +
		"<input type='radio' name='tech' id='tech_no' value='no' /> <label for='tech_no'>" + "<?php echo _('No') ?>" + "</label> </p>" +
		"<textarea class='comments_box' id='technical' name='technical' style='display:none;' onfocus='onFocusBox(this);' onblur='onBlurBox(this);'>" + 
		"<?php echo _('You may leave this area blank.'); ?>" + "</textarea>" +
		"<p>" + "<?php echo _('Did you cheat or in any way provide false information? If yes, how?');?>" + " &nbsp; " +
		"<input type='radio' name='cheat' id='cheat_yes' value='yes' /> <label for='cheat_yes'>" + "<?php echo _('Yes') ?>" + "</label> " +
		"<input type='radio' name='cheat' id='cheat_no' value='no' /> <label for='cheat_no'>" + "<?php echo _('No') ?>" + "</label> </p>" +
		"<textarea class='comments_box' id='cheating' name='cheating' style='display:none;' onfocus='onFocusBox(this);' onblur='onBlurBox(this);'>" + 
		"<?php echo _('You may leave this area blank.'); ?>" + "</textarea>"
		+ "</form><br/>" +
		"<?php echo _('Alternatively, you may email us at <b>info@labinthewild.org</b>.<br/>'); ?>";

	$("input[name=tech]").click( function() {
		if ($$$('tech_yes').checked)
			$$$("technical").style.display = 'block';
		else
			$$$("technical").style.display = 'none';
	});
	
	$("input[name=cheat]").click( function() {
		if ($$$('cheat_yes').checked)
			$$$("cheating").style.display = 'block';
		else
			$$$("cheating").style.display = 'none';
	});
	
	showSlide("comments");
	showNextButton(submitComments);
	disableKeyboard();
}


// functions to control how the comment boxes display
function onFocusBox (box) {
	if (box.value === <? echo _("'You may leave this area blank.'"); ?>) {
		box.value = '';
		box.style.color = "#333";
	}
}
function onBlurBox (box) {
	if (box.value === '') {
		box.value = <? echo _("'You may leave this area blank.'"); ?>;
		box.style.color = "#999";
	}
}

// function to submit the comments about the test to the server
function submitComments() {
	// validate form
	if ($("input[name=cheat]:checked").length == 0 || $("input[name=cheat]:checked").length == 0) {
		alert(<?php echo _("'Please tell us whether you encountered technical difficulties or cheated.'"); ?>);
		return;
	}
	
	$$$("ajax_working").style.display = 'block';
	$$$("ajax_working").style.top = '-85px';
	$$$("comments").style.opacity = '0.3';
	$$$("next_button").style.opacity = '0.3';
	
	// process the responses
	var general = ($$$("general").value == <? echo _("'You may leave this area blank.'"); ?>) ? "" : $$$("general").value;
	var cheating, technical;
	if ($$$("cheat_yes").checked)
		cheating = ($$$("cheating").value == <? echo _("'You may leave this area blank.'"); ?>) ? "Yes" : $$$("cheating").value;
	else
		cheating = "";
	if ($$$("tech_yes").checked)
		technical = ($$$("technical").value == <? echo _("'You may leave this area blank.'"); ?>) ? "Yes" : $$$("technical").value;
	else
		technical = "";
	var ma = Math.random();
	var ind = Math.floor(ma * 3);


	// for the country comparison version (ind == 1), determine if participant is from one of the countries we have averages for. If not, rest ind to the global comparison condition
	if (ind == 1)
		if (!(browser_lang == "en" && (vars.participant_country == "United States" || vars.participant_country == "China" || vars.participant_country == "United Kingdom" || vars.participant_country == "Canada" || vars.participant_country == "India")))
			ind = 2;

	// AJAX request for the rest of the comments data
  $.ajax({
  	type: 'POST',
  	url: 'includes/comments.php',
  	data: {
  		index:ind,
  		general: general,
  		cheating: cheating,
  		technical: technical,
  		participant_id: vars.participant_id
  	}
  }).done(function () {
  	setTimeout(finish(ind), 1000);
  });
}