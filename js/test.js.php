  /*************************************************************
 * combined_test.js.php
 *
 * Contains functions and configuration variables that run 
 * the "analytic-holistic" experiment.
 * 
 * Author: Eunice Jun
 * Based on code by Yuechen Zhao, Trevor Croxson (most recently)
 * Last modified: November, 2016
 * 
 * © Copyright 2014 Lab in the Wild
 * For questions about this file and permission to use
 * the code, contact us at info@labinthewild.org
 *************************************************************/

<? 
include "../localization.php"; 
?>

// controls the number of trials in each phase
// var exp_control = {1: 0, 2: 1, 3: 1};
var exp_control = {1: 10, 2: 14, 3: 10}; 
//var exp_control = {1: 2, 2: 2, 3: 2};
var num_phases = 3;

var num_trials = 1;
var exp_phase = 1;
var overall_trial_num = 0;
var content_trial_num = 0;
var trial;
var total_trials = exp_control["1"] + exp_control["2"] + exp_control["3"];

//browser language
var browser_lang;


var vars = {
	participant_id: 0,
	participant_country: "Never-Neverland"
}

var triad_vars = {
	// variable that we use to hold the event chain
	ch: null,
	// the words the user has selected at any given time
	selected_words: [false,false,false]
};

var ia_vars = {
	// variable that we use to hold the event chain
	ch: null,
	// whether the user has been reminded about troubleshooting tips
	rtWarned: false,
	selected_question: null
};

var triad_config = {
	stimDuration: 1000,
	ITIDuration: 250,
	respDuration: 20000
};

var ia_config = {
	stimDuration: 1000,
	ITIDuration: 250,
	respDuration: 20000
};

// all the text that is ever displayed to the user
var text = 
(withTouch) ?
{
	instPrac: "<? echo _('<h3>Practice</h3>Tap the image set where you feel the prompt image belongs:'); ?>",

	instPrac2: "<? echo _('Good job! You\'ve reached the second portion of the test. For this part of the test, you\'ll be shown two sets of images.'); ?>" + "<br><br>"
		+ "<? echo _('Between these images, there will be an additional prompt image similar to the ones on either side.<br><br>Please decide which image set the prompt belongs with by tapping on the appropriate image set.<br><br>Please try to answer as quickly as possible.'); ?>"
		+ "<br><br>" + "<? echo _('Please tap the screen to continue.'); ?>",
	inst2: "<? echo _('That concludes the practice portion of this part of the test. Now that you understand the task, the actual test will begin.'); ?>"
		+ "<br><br><img src='images/stimImgNew.png' id='stimImg' class='insts'/><br>"
		+ "<? echo _('Please tap the screen to continue.'); ?>",
	troubleshooting: "<? echo _('If you are having trouble responding, make sure you are using a modern browser. If you are not certain, use your mobile device\'s default browser.'); ?>"
	  	+ "<br><br>" + "<? echo _('Please lightly tap the image to select.<br><br>If this problem persists, please contact us and let know of your mobile device\'s name and software version.<br><br>This message will not appear again.'); ?>"
	  	+ "<? echo _('Please tap the screen to continue.'); ?>"
} :
{
	instPrac: "<? echo _('<h3>Practice</h3>Click on the image set where you feel the prompt image belongs:'); ?>",

	instPrac2: "<? echo _('Good job! You\'ve reached the second portion of the test. For this part of the test, you\'ll be shown two sets of images.'); ?>" + "<br><br>"
		+ "<? echo _('Between these images, there will be an additional prompt image similar to the ones on either side.<br><br>Please decide which image set the prompt belongs with by clicking on the appropriate image set.<br><br>Please try to answer as quickly as possible.'); ?>",
	inst2: "<br><br><br>" + "<? echo _('That concludes the practice portion of this part of test.<br><br>Now that you understand the task, the actual task will begin.'); ?>" + "<br><br>",
	troubleshooting: "<? echo _('If you are having trouble responding, you may also use the number keys on your keyboard to select an image. Use number keys 1 and 2 to select the left or right image set.<br><br>This message will not appear again.'); ?>" + "<br><br>"
};

// experiment object holds all the information necessary for the experiment, records data
var ia_experiment = {
	// trials will hold all the information for each trial
	trials: [],
	// results will hold all of the results that get submitted at the end of the experiment
	results: [],
	
	// blankImg is the generic name we use for the inter-stimulus non-fixation cross image
	blankImg: "images/white.jpg",
	
	left_question: null,
	right_question: null,
	prompt: null,

	// the simuli
	stims: 
		[
			["", "formal_intuitive_img/1-1.png", "formal_intuitive_img/2-1.png", "formal_intuitive_img/3-1.png", "formal_intuitive_img/4-1.png", "formal_intuitive_img/5-1.png", "formal_intuitive_img/6-1.png", "formal_intuitive_img/7-1.png", "formal_intuitive_img/1-1.png", "formal_intuitive_img/2-1.png", "formal_intuitive_img/3-1.png", "formal_intuitive_img/4-1.png", "formal_intuitive_img/5-1.png", "formal_intuitive_img/6-1.png", "formal_intuitive_img/7-1.png"],
			["", "formal_intuitive_img/1-2.png", "formal_intuitive_img/2-2.png", "formal_intuitive_img/3-2.png", "formal_intuitive_img/4-2.png", "formal_intuitive_img/5-2.png", "formal_intuitive_img/6-2.png", "formal_intuitive_img/7-2.png", "formal_intuitive_img/1-2.png", "formal_intuitive_img/2-2.png", "formal_intuitive_img/3-2.png", "formal_intuitive_img/4-2.png", "formal_intuitive_img/5-2.png", "formal_intuitive_img/6-2.png", "formal_intuitive_img/7-2.png"],
			["", "formal_intuitive_img/1-3.png", "formal_intuitive_img/2-3.png", "formal_intuitive_img/3-3.png", "formal_intuitive_img/4-3.png", "formal_intuitive_img/5-3.png", "formal_intuitive_img/6-3.png", "formal_intuitive_img/7-3.png", "formal_intuitive_img/1-4.png", "formal_intuitive_img/2-4.png", "formal_intuitive_img/3-4.png", "formal_intuitive_img/4-4.png", "formal_intuitive_img/5-4.png", "formal_intuitive_img/6-4.png", "formal_intuitive_img/7-4.png"]
		],

	// array to preload resources
	preloadArr:
		[
			"formal_intuitive_img/1-1.png", "formal_intuitive_img/1-2.png", "formal_intuitive_img/1-3.png", "formal_intuitive_img/1-4.png",
			"formal_intuitive_img/2-1.png", "formal_intuitive_img/2-2.png", "formal_intuitive_img/2-3.png", "formal_intuitive_img/2-4.png",
			"formal_intuitive_img/3-1.png", "formal_intuitive_img/3-2.png", "formal_intuitive_img/3-3.png", "formal_intuitive_img/3-4.png",
			"formal_intuitive_img/4-1.png", "formal_intuitive_img/4-2.png", "formal_intuitive_img/4-3.png", "formal_intuitive_img/4-4.png",
			"formal_intuitive_img/5-1.png", "formal_intuitive_img/5-2.png", "formal_intuitive_img/5-3.png", "formal_intuitive_img/5-4.png",
			"formal_intuitive_img/6-1.png", "formal_intuitive_img/6-2.png", "formal_intuitive_img/6-3.png", "formal_intuitive_img/6-4.png",
			"formal_intuitive_img/7-1.png", "formal_intuitive_img/7-2.png", "formal_intuitive_img/7-3.png", "formal_intuitive_img/7-4.png"
		],

	trialTypes: 
		["instPrac2","test","test","test","test","test","test","test","test","test","test","test","test","test","test"],

		 // "1" and "2" refer to the left and right question stims
    ia_stims_answers: [
   		{1: "na",
   		 2: "na"},
   		{1: "analytic",
   		 2: "holistic"}, //ok
   		{1: "analytic",
   		 2: "holistic"}, //ok
   		{1: "analytic",
   		 2: "holistic"}, //ok
   		{1: "analytic",
   		 2: "holistic"}, //ok
   		{1: "holistic",
   		 2: "analytic"}, //ok
   		{1: "analytic",
   		 2: "holistic"}, //ok
   		{1: "holistic",
   		 2: "analytic"}, //ok
   		{1: "holistic",
   		 2: "analytic"}, //ok
   		{1: "holistic",
   		 2: "analytic"}, //ok
   		{1: "holistic",
   		 2: "analytic"}, //ok
   		{1: "holistic",
   		 2: "analytic"}, //ok
   		{1: "analytic", 
   		 2: "holistic"}, //ok
   		{1: "holistic", 
   		 2: "analytic"}, //ok
   		{1: "analytic",
   		 2: "holistic"}  //ok
   	]
};

var triad_experiment = {
	trials: [],
	results: [],
	left_word_stim: null,
	center_word_stim: null,
	right_word_stim: null,
	blankText: "",
	triad_stims: [
		["<? echo _('Seagull'); ?>", "<? echo _('Sky'); ?>", "<? echo _('Dog'); ?>"],
		["<? echo _('Black'); ?>", "<? echo _('White'); ?>", "<? echo _('Blue'); ?>"],
		["<? echo _('Doctor'); ?>", "<? echo _('Teacher'); ?>", "<? echo _('Homework'); ?>"],
		["<? echo _('Apple'); ?>", "<? echo _('Orange'); ?>", "<? echo _('Pear'); ?>"],
		["<? echo _('Shoes'); ?>", "<? echo _('Boots'); ?>", "<? echo _('Slippers'); ?>"],
		["<? echo _('Train'); ?>", "<? echo _('Bus'); ?>", "<? echo _('Tracks'); ?>"],
		["<? echo _('Computer monitor'); ?>", "<? echo _('Antenna'); ?>", "<? echo _('Television'); ?>"],
		["<? echo _('Hospital'); ?>", "<? echo _('Bank'); ?>", "<? echo _('Cinema'); ?>"],
		["<? echo _('Carrot'); ?>", "<? echo _('Eggplant'); ?>", "<? echo _('Rabbit'); ?>"],
		["<? echo _('Cloud'); ?>", "<? echo _('Wind'); ?>", "<? echo _('Rain'); ?>"],
		["<? echo _('Panda'); ?>", "<? echo _('Banana'); ?>", "<? echo _('Monkey'); ?>"],
		["<? echo _('Shirt'); ?>", "<? echo _('Hat'); ?>", "<? echo _('Pants'); ?>"],
		["<? echo _('Kite'); ?>", "<? echo _('Basketball'); ?>", "<? echo _('Tennis'); ?>"],
		["<? echo _('Farmer'); ?>", "<? echo _('Corn'); ?>", "<? echo _('Bread'); ?>"],
		["<? echo _('Shampoo'); ?>", "<? echo _('Hair'); ?>", "<? echo _('Beard'); ?>"],
		["<? echo _('Bridge'); ?>", "<? echo _('Tunnel'); ?>", "<? echo _('Highway'); ?>"],
		["<? echo _('Piano'); ?>", "<? echo _('Violin'); ?>", "<? echo _('Guitar'); ?>"],
		["<? echo _('Child'); ?>", "<? echo _('Man'); ?>", "<? echo _('Woman'); ?>"],
		["<? echo _('Postman'); ?>", "<? echo _('Policeman'); ?>", "<? echo _('Uniform'); ?>"],
		["<? echo _('Letter'); ?>", "<? echo _('Stamp'); ?>", "<? echo _('Postcard'); ?>"]
	],
	// a separate array of the stims in English, used for recording user responses in the database
	triad_stims_english: [
		["Seagull", "Sky", "Dog"],
		["Black", "White", "Blue"],
		["Doctor", "Teacher", "Homework"],
		["Apple", "Orange", "Pear"],
		["Shoes", "Boots", "Slippers"],
		["Train", "Bus", "Tracks"],
		["Computer monitor", "Antenna", "Television"],
		["Hospital", "Bank", "Cinema"],
		["Carrot", "Eggplant", "Rabbit"],
		["Cloud", "Wind", "Rain"],
		["Panda", "Banana", "Monkey"],
		["Shirt", "Hat", "Pants"],
		["Kite", "Basketball", "Tennis"],
		["Farmer", "Corn", "Bread"],
		["Shampoo", "Hair", "Beard"],
		["Bridge", "Tunnel", "Highway"],
		["Piano", "Violin", "Guitar"],
		["Child", "Man", "Woman"],
		["Postman", "Policeman", "Uniform"],
		["Letter", "Stamp", "Postcard"]
	],
	triad_stims_answers: [
		{a: [true,false,true],		//["Seagull", "Sky", "Dog"],
		 h: [true,true,false]},
		{a: [false,false,false],      //["Black", "White", "Blue"] (filler),
		 h: [false,false,false]},
		{a: [true,true,false],		//["Doctor", "Teacher", "Homework"],
		 h: [false,true,true]},
		{a: [false,false,false],		//["Apple", "Orange", "Pear"] (filler),
		 h: [false,false,false]},
		{a: [false,false,false],		//["Shoes", "Boots", "Slippers"] (filler),
		 h: [false,false,false]},
		{a: [true,true,false],		//["Train", "Bus", "Tracks"],
		 h: [true,false,true]},
		{a: [true,false,true],		//["Computer monitor", "Antenna", "Television"],
		 h: [false,true,true]},
		{a: [false,false,false],		//["Hospital", "Bank", "Cinema"] (filler),
		 h: [false,false,false]},
		{a: [true,true,false],		//["Carrot", "Eggplant", "Rabbit"],
		 h: [true,false,true]},
		{a: [false,true,true],		//["Cloud", "Wind", "Rain"],
		 h: [true,false,true]},
		{a: [true,false,true],		//["Panda", "Banana", "Monkey"],
		 h: [false,true,true]},
		{a: [false,false,false],		//["Shirt", "Hat", "Pants"] (filler),
		 h: [false,false,false]},
		{a: [false,false,false],		//["Kite", "Basketball", "Tennis"] (filler),
		 h: [false,false,false]},
		{a: [false,false,false],		//["Farmer", "Corn", "Bread"] (filler),
		 h: [false,false,false]},
		{a: [false,true,true],		//["Shampoo", "Hair", "Beard"],
		 h: [true,true,false]},
		{a: [false,false,false],		//["Bridge", "Tunnel", "Highway"] (filler),
		 h: [false,false,false]},
		{a: [false,false,false],		//["Piano", "Violin", "Guitar"] (filler),
		 h: [false,false,false]},
		{a: [false,false,false],		//["Child", "Man", "Woman"] (filler),
		 h: [false,false,false]},
		{a: [true,true,false],		//["Postman", "Policeman", "Uniform"],
		 h: [false,true,true]},
		{a: [true,false,true],		//["Letter", "Stamp", "Postcard"]
		 h: [true,true,false]}
	]
}

// shuffles any number of equal-length arrays in the same way.
// can optionally pass an offset value as the first argument to ignore the first x elements of the array
function shuffleMultiArray() {
	// check to see if an offset is passed
	if (typeof(arguments[0]) == "number") {
		var offset = arguments[0];
		var i = 1;
		var counter = (arguments[1].length - offset), temp, index;
	} else {
		var offset = 0;
		var i = 0;
		var counter = arguments[0].length, temp, index;
	}
    while (counter > 0) {
        index = Math.floor(Math.random() * counter);
        counter--;
        for (var j = i; j < arguments.length; j++) {
	        temp = arguments[j][counter + offset];
	        arguments[j][counter + offset] = arguments[j][index + offset];
	        arguments[j][index + offset] = temp;
    	}
    }
}

//In the results, "analysis" refers to whether the user selected the analytic combo of words, the holistic combo, or the incorrect combo
function recordTriadData(input, trial) {
	// if there's a chain of timeouts, clear them all
	if (triad_vars.ch) clearChain(triad_vars.ch);

	trial.end_time = new Date();

	// check if this stim is a filler
	if (arraysEqual(trial.answers.a, [false,false,false])) input.analysis = "filler";
	// for the non-filler stims, check to see if the response is analytic, holistic, or neither
	else if (arraysEqual(triad_vars.selected_words, trial.answers.a)) input.analysis = "analytic";
	else if (arraysEqual(triad_vars.selected_words, trial.answers.h)) input.analysis = "holistic";
	else input.analysis = "neither";

	// push the indices of selected words to a temp array
	var temp = [];
	for (var i = 0; i < triad_vars.selected_words.length; i++) {
		if (triad_vars.selected_words[i]) temp.push(i);
	}

	triad_experiment.results.push({
		type: "triad",
		phase: exp_phase,
		overall_trial_num: overall_trial_num,
		left_word_stim: trial.stims_english[0],
		center_word_stim: trial.stims_english[1],
		right_word_stim: trial.stims_english[2],
		response_1: trial.stims_english[temp[0]],
		response_2: trial.stims_english[temp[1]],
		analysis: input.analysis,
		rt: trial.end_time - trial.start_time
	});

	//reset selected words and stim borders:
	triad_vars.selected_words = [false,false,false];
	triad_experiment["left_word_stim"].style.border = "5px solid transparent";
	triad_experiment["center_word_stim"].style.border = "5px solid transparent";
	triad_experiment["right_word_stim"].style.border = "5px solid transparent";
	$$$("continue").style.display = "none";

	nextTrial();
}

// function that pushes one trial's worth of intuitive-analytic data onto the results array:
function recordIaData(input, trial) {
	ia_experiment.results.push({
		type: "ia_" + trial.type,
		phase: exp_phase,
		left_question: trial.left_question,
		right_question: trial.right_question,
		prompt: trial.prompt,
		response: input.response,
		analysis: (input.response == 6) ? "N/A" : trial.ia_stims_answers[input.response],
		rt: trial.end_time - trial.start_time,
		overall_trial_num: overall_trial_num
	});
}

function arraysEqual(a, b) {
	for (var i = 0; i < a.length; i++) {
		if (a[i] != b[i]) return false;
	}
	return true
}

function setup() {
	(withTouch) ? $("#word_stim_title").html( "<p>" + "<? echo _('Tap on the two words you feel go together best.'); ?>" + "</p><p>" + "<? echo _('Please try to answer as quickly as possible.'); ?>" + "</p>") : $("#word_stim_title").html( "<p>" + "<? echo _('Using your mouse, click on the two words you feel go together best.'); ?>" + "</p><p>" + "<? echo _('Please try to answer as quickly as possible.'); ?>" + "</p>");
	$$$("continue").style.display = "none";
	$$$("word_stims_box").style.display = "none";
	$$$("ajax_working").innerHTML = getAJAXWorkingHTML();

	// attach event listeners for word stims:
	$("#left_word_stim").hover(function() {
		$(this).css("background", "#DFEFFE")
			   .css("cursor", "pointer");
	}, function() {
		$(this).css("background", "#FFFFFF")
	})
	.on("click", function() {
		processTriadResponse({response: 1}, trial);
	});
	$("#center_word_stim").hover(function() {
		$(this).css("background", "#DFEFFE")
			   .css("cursor", "pointer");
	}, function() {
		$(this).css("background", "#FFFFFF")
	})
	.on("click", function() {
		processTriadResponse({response: 2}, trial);
	});
	$("#right_word_stim").hover(function() {
		$(this).css("background", "#DFEFFE")
		       .css("cursor", "pointer");
	}, function() {
		$(this).css("background", "#FFFFFF")
	})
	.on("click", function() {
		processTriadResponse({response: 3}, trial);
	});

	// attach event listener for next button on triad phase
	$("#continue").on("click", function() {
		recordTriadData({response: 4}, trial);
	});

	// attach event listeners for intuitive-analytic stims
	$("#ia_left_question").hover(function() {
		$(this).css("background", "#afd6fd")
			   .css("cursor", "pointer");
	}, function() {
		$(this).css("background", "#FFFFFF")
	})
	.on("click", function() {
		processIaResponse({response: 1}, trial);
	});
	$("#ia_right_question").hover(function() {
		$(this).css("background", "#afd6fd")
			   .css("cursor", "pointer");
	}, function() {
		$(this).css("background", "#FFFFFF")
	})
	.on("click", function() {
		processIaResponse({response: 2}, trial);
	});


	// set up instructions for intuitive-analytic portion of the experiment
	$$$("instPrac2text").innerHTML = text.instPrac2;
	$$$("instPrac3text").innerHTML = text.instPrac3;
	$$$("inst2text").innerHTML = text.inst2;

	// grab image elements for intuitive-analytic portion
	ia_experiment.left_question = $$$("ia_left_question");
	ia_experiment.right_question = $$$("ia_right_question");
	ia_experiment.prompt = $$$("ia_prompt");

	// grab divs for the triad portion:
	triad_experiment.left_word_stim = $$$("left_word_stim");
	triad_experiment.center_word_stim = $$$("center_word_stim");
	triad_experiment.right_word_stim = $$$("right_word_stim");

	// shuffle order of each triad stim (and corresponding answers)
	for (var i = 0; i < triad_experiment.triad_stims.length; i++) {
		shuffleMultiArray(triad_experiment.triad_stims[i], triad_experiment.triad_stims_english[i], triad_experiment.triad_stims_answers[i].a, triad_experiment.triad_stims_answers[i].h);
	}

	// shuffle order of triads
	shuffleMultiArray(triad_experiment.triad_stims, triad_experiment.triad_stims_english, triad_experiment.triad_stims_answers);

	// shuffle order of analytic-intuitve stims, with offset of 1 to avoid shuffling instruction content
	shuffleMultiArray(1, ia_experiment.stims[0], ia_experiment.stims[1], ia_experiment.stims[2], ia_experiment.ia_stims_answers);

	// populate the trials array for the triad portion
	for(var i = 0; i < triad_experiment.triad_stims.length; i++) {
		triad_experiment.trials.push({
			num: i + 1,
			stims: triad_experiment.triad_stims[i],
			stims_english: triad_experiment.triad_stims_english[i],
			left_word_stim: triad_experiment.triad_stims[i][0],
			center_word_stim: triad_experiment.triad_stims[i][1],
			right_word_stim: triad_experiment.triad_stims[i][2],
			answers: triad_experiment.triad_stims_answers[i],
			start_time: 0,
			end_time: 0
		});
	};

	// populate the trials array for the intuitive-analytic portion
	for(var i = 0; i < ia_experiment.trialTypes.length; i++) {
		ia_experiment.trials.push({
			type: ia_experiment.trialTypes[i],
			num: i + 1,
			left_question: ia_experiment.stims[0][i],
			right_question: ia_experiment.stims[1][i],
			prompt: ia_experiment.stims[2][i],
			ia_stims_answers: ia_experiment.ia_stims_answers[i],
			start_time: 0,
			end_time: 0
		});
	};
	// async preload all images for the intuitive-analytic portion
	$.ajax(preload(ia_experiment.preloadArr, function(){}));

	studyInfo();
}

// function to decide which trial to present next--intuitive-analytic or triad
function nextTrial() {
	
	//check to see if we have completed all the trials for this phase:
	if (num_trials > exp_control[exp_phase]) {
		num_trials = 1;
		exp_phase++;

		if (exp_phase > num_phases) {
			$("#trialNum").css("visibility", "hidden");
			showDemographics();
			return;
		}
	}

	$("#next_button").css("display", "none");
	$("#trialNum").html("<p><b>" + "<? echo _('Phase: '); ?></b>" + exp_phase + "<? echo _(' of '); ?>" + num_phases + "</p><p><b>" + "<? echo _('Progress: '); ?></b>" + num_trials + "<? echo _(' of '); ?>" + exp_control[exp_phase] + "</p>");

	//check to see if we're in a triad (even) or intuitive-analytic (odd) phase:
	if (exp_phase % 2 !== 0) {
			num_trials++;
			nextTriadTrial();
	} else {
			nextIaTrial();
	}
}

function nextTriadTrial() {
	overall_trial_num++;
	content_trial_num++;

	$$$("word_stims_box").style.display = "block";
	$$$("ia_content").style.display = "none";

	showSlide("word_stims_box");

	trial = triad_experiment.trials.shift();
	
	trial.start_time = new Date();

	triad_vars.ch = chain(
		function() {
			hideTriadStims();
		}, triad_config.ITIDuration,
		function() {
			showTriadStims(trial);
			triad_config.respKeys = (withTouch) ? [] : [1, 2, 3];
			getKeyboardInput(triad_config.respKeys, processTriadResponse, trial);
		}
	);
 }

function nextIaTrial() {
	overall_trial_num++;
	content_trial_num++;

	$$$("word_stims_box").style.display = "none";
	$$$("ia_content").style.display = "block";

	// set keys that can be used for the intuitive-analytic trials:
	ia_config.respKeys = (withTouch) ? [] : {'highlight': true, 1: $$$("ia_left_question"), 2: $$$("ia_right_question")};
	ia_config.clickObjs = (withTouch) ? [$$$("ia_left_question"), $$$("ia_right_question")] : [];	

	// Shift the next trial's information off the front of the trials array
	if (trial = ia_experiment.trials.shift()) {
		trial.start_time = new Date();

		if (trial.type == "test") num_trials++;
		
		// Show instructions, wait for user response
		if (trial.type === "instPrac1" || trial.type ==="instPrac2" || trial.type === "instPrac3" || trial.type === "inst") {
			content_trial_num--;

			$$$("headText").innerHTML = "";
			$("#trialNum").css("display", "none");
			
			// if touch, force orientation
			if (trial.type === "instPrac1" && withTouch) {
				window.setTimeout("fixOrientation('landscape')", 500);
			}
			showNextButton(function() {
				processIaResponse({response: 6}, trial)
			});

			showSlide(trial.type);
		} 

		// The testing stage -- wait for user input, and do not continue until input is given
		else {
			// Show the correct heading, whether we're in the practice stage or not
			if (trial.type === "prac_test") {
				content_trial_num--
				$$$("trialNum").innerHTML = "";
				$$$("headText").innerHTML = text.instPrac;
			} else {
				(withTouch) ? $$$("headText").innerHTML = "<? echo _('<strong>Tap on the group of images where you feel the center image belongs.</strong>'); ?>" : $$$("headText").innerHTML = "<? echo _('<strong>Using your mouse, click on the group of images where you feel the center image belongs.</strong>'); ?>";
			}
			
			$("#trialNum").css("display", "block");
		    $("#question").css("height", "200px");
		    $("#question").css("width", "auto");

			// create the chain of timeouts for this testing trial
			ia_vars.ch = chain (
						// show blank first
						function () {
							hideIaStims();
							$("#next_button").css("display", "none");
							showSlide("empty");
						}, ia_config.ITIDuration,
						// start timing, show pictures, wait for touch or keyboard input
						function() {
							showIaStims(trial); 
							showSlide("stimDiv");

							ia_vars.clickRtStart = new Date();
							if (!withTouch)
								getKeyboardInput(ia_config.respKeys, processIaResponse, trial);
							else
								getKeyboardInput({ highlight: false, 1: $$$("ia_left_question"), 2: $$$("ia_right_question")}, processIaResponse, trial);
						}
					);
		}
	}
}

// clears the current trial completely without recording data; useful for warnings and errors
function clearTrial() {
	// hide stimuli
	hideIaStims();
	// if there's a chain of timeouts, clear them all
	if (ia_vars.ch)
		clearChain(ia_vars.ch);
	// no more input is needed, so stop all listeners
	/*
	if (withTouch) {
		disableTouch(ia_config.clickObjs);
		disableTouch("document");
	}
	else {
		disableKeyboard();
	}
	*/
	
	// and this trial is done; go to the next intuitive-analytic trial.
	nextIaTrial();
}

function showTriadStims(trial) {
    $$$("left_word_stim").innerHTML = trial.left_word_stim;
    $$$("center_word_stim").innerHTML = trial.center_word_stim;
    $$$("right_word_stim").innerHTML = trial.right_word_stim;
}

// show the stimuli
function showIaStims (trial) {
    ia_experiment.left_question.src = trial.left_question;
    ia_experiment.right_question.src = trial.right_question;
	ia_experiment.prompt.src = trial.prompt;
}

function hideTriadStims() {
	triad_experiment.left_word_stim.innerHTML = triad_experiment.center_word_stim.innerHTML = triad_experiment.right_word_stim.innerHTML = triad_experiment.blankText;
}

function hideIaStims() {
	ia_experiment.left_question.src = ia_experiment.right_question.src = ia_experiment.prompt.src = ia_experiment.blankImg;
}

function processTriadResponse(input, trial) {

	// the name of the div is now the value of input	
	var selected_div;
	switch(input.response) {
		case 1:
			selected_div = "left_word_stim";
			break;
		case 2:
			selected_div = "center_word_stim";
			break;
		case 3:
			selected_div = "right_word_stim";
			break;
		case 4:
			selected_div = "continue";
			break;
	};

	/*
	// no more input is needed, so stop all listeners
	if (withTouch) {
		disableTouch(triad_config.clickObjs);
		disableTouch("document");
	} else {
		disableKeyboard();
	}
	*/



	//invert selection status and border for the element the user has selected:
	if (input.response != "space") {
		triad_vars.selected_words[input.response - 1] = !triad_vars.selected_words[input.response - 1];
		(triad_vars.selected_words[input.response - 1] == true) ? triad_experiment[selected_div].style.border = "5px solid #afd6fd" : triad_experiment[selected_div].style.border = "5px solid transparent";
	}

	//check to see if we're in a state where the user can progress to the next trial (i.e. exactly two items selected):
	if (triad_vars.selected_words.filter(function(x){return x == true}).length == 2) {
		//check to see if user has pressed the space bar to continue to the next trial:
		if (input.response == "space") {
			//num_trials++;
			recordTriadData(input, trial);
			return;
		}
		//add the space bar to accepted keys
		triad_config.respKeys = (withTouch) ? [] : [1, 2, 3, "space"];
		$$$("continue").style.display = "block";
		getKeyboardInput(triad_config.respKeys, processTriadResponse, trial);

	} else {
		$$$("continue").style.display = "none";
		triad_config.respKeys = (withTouch) ? [] : [1, 2, 3];
		getKeyboardInput(triad_config.respKeys, processTriadResponse, trial);
	}
}

function processIaResponse(input, trial) {
	switch (input.response) {
	    case "ia_left_question":
			input.response = 1;
			break;
		case "ia_right_question":
			input.response = 2;
			break;
		case "document":
			input.response = 6;
			break;
		case "space":
			input.response = 6;
			break;
	}

	// no more input is needed, so stop all listeners
	/*
	if (withTouch) {
		disableTouch(ia_config.clickObjs);
		disableTouch("document");
	}
	else {
		disableKeyboard();
	}
	*/

	// hide stimuli
	hideIaStims();
	// if there's a chain of timeouts, clear them all
	if(ia_vars.ch)
		clearChain(ia_vars.ch);
	
	trial.end_time = new Date();

	// record data
	recordIaData(input, trial);

	// and this trial is done; go to the next trial.
	nextTrial();
}

function processData(analytic, holistic) {
	$.ajax({
    	type: 'POST',
    	url: 'includes/data.php',
    	data: {
    		participant_id: vars.participant_id,
    		ia_results: JSON.stringify(ia_experiment.results),
    		triad_results: JSON.stringify(triad_experiment.results),
            analytic_score: analytic,
            holistic_score: holistic
    	}
    });
}

function showDemographics() {
	//$("#header").hide();
	showSlide("demographics");
	showNextButton(submit);
	$("#trialNum").remove();
	demographics();
}

function showNextButton(fun) {
	if (fun instanceof Function) {
		$$$("next_button").innerHTML = "<p>" + "<? echo _('Click the arrow to continue:'); ?>" + "</p><button type='button' id='next'> </button>";
		$("#next").on("click", fun)
				  .css("hover", "cursor:pointer");
	}
	getKeyboardInput(["space", "right"], fun);
	$$$("next_button").style.display = "block";
}

// global variables that indicate participant's analytic and holistic scores
var totalAnalytic, totalHolistic;
function calculateParticipantScores() {
    // scores for the "3 words" portion of the study
    var triad_analytic = 0, triad_holistic = 0;
    for (var i = 0; i < triad_experiment.results.length; i++) {
        if (triad_experiment.results[i]["analysis"] == "analytic") {
            triad_analytic++;
        } else if (triad_experiment.results[i]["analysis"] == "holistic") {
            triad_holistic++;
        }
    }

    // score for the image portion of the study
    var ia_analytic = 0, ia_holistic = 0;
    for (var i = 0; i < ia_experiment.results.length; i++) {
        if (ia_experiment.results[i]["analysis"] == "analytic") {
            ia_analytic++;
        } else if (ia_experiment.results[i]["analysis"] == "holistic") {
            ia_holistic++;
        }
    }

    // final participant analytic and holistic scores
    totalAnalytic = triad_analytic + ia_analytic;
    totalHolistic = triad_holistic + ia_holistic;

    //submit experiment data to server:
    processData(totalAnalytic, totalHolistic);
    //hard-coded overall average:
    var avg_score = 0;
};

  // global variable representing participant's country, used to access/refer to
  // various country-specific stats, values stored in arrays
  // countriesArray[countryIndex] == country's stat, value, etc. throughout the experiment
  // if countryIndex == 0, using global stat, value, etc.
  var countryIndex;
  // @return newly assigned countryIndex
  function assignCountryIndex() {
//	  console.log(vars.participant_country);
//	  console.log(browser_lang);
      if ((vars.participant_country == "Canada") && (browser_lang == "en")) {
          countryIndex = 1;
      }
      else if ((vars.participant_country == "China") && (browser_lang == "en")) {
          countryIndex = 2;
      }
      else if ((vars.participant_country == "Germany") && (browser_lang == "en")) {
          countryIndex = 3;
      }
      else if ((vars.participant_country == "India") && (browser_lang == "en")) {
          countryIndex = 4;
      }
      else if ((vars.participant_country == "Japan") && (browser_lang == "en")) {
          countryIndex = 5;
      }
      else if ((vars.participant_country == "United Kingdom") && (browser_lang == "en")) {
          countryIndex = 6;
      }
      else if ((vars.participant_country == "United States") && (browser_lang == "en")) {
          countryIndex = 7;
      }
      else {
          countryIndex = 0;
      }

      return countryIndex;
  };

// Array containing the range of scores that capture the middle <=30% of participants in the distributions of scores
// indexing corresponds to global array of countryMeans
// Previous implementations of this study had an overall score that was totalHolistic - totalAnalytic
// so these score ranges use the overals scores
  var countryRanges = [
      [3,9], // WORLD
      [5,11], // Canada
      [1,7], // China
      [3,9], // Germany
      [5,11], // India
      [1,7],// Japan
      [-1,5], // UK
      [3,9] // US
  ];

  // Global variable for the condition the participant' score assigns them to
  // 0 = similar; 1 = different
  var condition = -1;
  // @precondition: global totalHolistic and totalAnalytic variables must be intialized and assigned
  function assignCondition() {
      var tmp_overallScore = totalHolistic - totalAnalytic;

      var low = countryRanges[countryIndex][0];
      var high = countryRanges[countryIndex][1];

      if (tmp_overallScore >= low && tmp_overallScore <= high)
          condition = 0;
      else
          condition = 1;
  };

function finish() {
	var enter = new Date().getTime();

	// attach listeners to track hover time on results page graphs
	var resultsHoverTimeTotal = 0;
	var hoverSessionEnter = 0;
	var verifyMouseEnter = false;
	$("#results_canvas").on("mouseenter", function () {
		verifyMouseEnter = true;
		hoverSessionEnter = new Date().getTime();
	}).on("mouseout", function () {
		// guard against two mouse out events firing back to back (from rapid mouse movements) and corrupting resultsHoverTimeTotal
		if (verifyMouseEnter) {
			resultsHoverTimeTotal += new Date().getTime() - hoverSessionEnter;
			verifyMouseEnter = false;
			hoverSessionEnter = 0;
		}
	});

	$("#results_share").prepend("<span id='results_share_text'>" + <? echo _("'Share your results!'"); ?> +"</span>");

	$('html, body').animate({scrollTop: 0}, 0);
	$("#trialNum").css("visibility", "hidden");
	$("#next_button").css("display", "none");
	$("#ajax_working").css("display", "none");

//	enableKeyboard();//Yan add this

    assignCountryIndex();
//	console.log(countryIndex);
    calculateParticipantScores(); // must call this before assign condition because condition depends on score
//	totalAnalytic = 9;
//	totalHolistic = 15;
	processData(totalAnalytic, totalHolistic);
	assignCondition();
//	console.log(condition);
//	console.log(condition);
//	console.log(countryRanges[countryIndex]);


	$("#results").prepend(
		"<h2 class='bolded-blue'>" + "<?php echo _('Let\'s see how you did!'); ?>" + "</h2>");

	// results page visualization SVG (parent SVG)
	var results_svg = d3.select("#results_graph").append("svg")
		.attr("width", 800)
		.attr("height", 400)
		.attr("id", "results_svg");
	var label_width = 150;

	// results_area: the space in which we can render the visualization
	// creates a 20px border around the visualization
	var margin = {top: 30, right: 10, bottom: 0, left: 50},
		// width = results_svg.attr("width") - margin.left - margin.right,
		width = 300,
		// height = results_svg.attr("height") - margin.top  - margin.bottom,
		height = 300,
		results_area = results_svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	renderGraph(totalHolistic, totalAnalytic, width, height, results_area);
	var personal_assessment = getParticipantAssessment(totalHolistic, totalAnalytic);
	var comparison_assessment = getComparisonAssessment(condition);

	$("#results_graph_text").append("" +
		"<p>" + comparison_assessment + "</p>" +
		"<p>" + personal_assessment + "</p>");

	if (countryIndex != 0) // country comparison possible and shown
	{
		var footnote = "<? echo _('*The circle of other participants from your country represents approximately 1/3 of participants and is based on previous data.'); ?>";
	}
	else
	{
		var footnote = "<? echo _('*The circle of participants from the world represents approximately 1/3 of participants and is based on previous data.'); ?>";
	}



	// TEXT ACCOMPANYING RESULTS GRAPH
  $("#results").append(
	  //"<p>" + personal_assessment + "</p>" +
	  //"<p>" + comparison_assessment + "</p>" + "<br /><br />" +
	  "<div id='score_heading' class='btn btn-primary exp_headings'>" + "<? echo _('How did we determine your result?'); ?>" + "</div>" +
	  "<div id='score_exp_preview' class='results_exps'><p>" + "<? echo _('This test measured your thinking style. It\'s based on two previous studies that found cultural differences in the way people group information [1,2] (see references below).<br /><br />First, we tested whether you prefer to group words by their analytic category or by their holistic function, as below:'); ?>" +
	  "<button id='score_exp_more_button' class='btn exp_read_buttons'>" + "<? echo _('Read more'); ?>" + "</button></p></div>" +
	  "<div id='score_exp' class='results_exps'><p>" + "<? echo _('This test measured your thinking style. It\'s based on two previous studies that found cultural differences in the way people group information [1,2] (see references below).<br /><br />First, we tested whether you prefer to group words by their analytic category or by their holistic function, as below:'); ?>" + "</p>" +
	  "<table><tr><td><b>" + "<? echo _('Analytic combination'); ?>" + "</b></td><td style='width:40px'></td><td><b>" + "<? echo _('Holistic combination'); ?>" + "</b></td></tr><tr><td><img src='images/abstract_combo.png' style='width: 330px' /></td><td style='width:40px'></td><td><img src='images/functional_combo.png' style='width: 330px' /></td></tr><tr><td><em style='font-size: 12pt'>" + "<? echo _('\"Seagull\" and \"dog\" both belong to the same abstract category: animals.'); ?>" + "</em></td><td style='width:40px'></td><td><em style='font-size: 12pt'>" + "<? echo _('\"Seagull\" and \"sky,\" however, are grouped together by their function: seagulls fly in the sky.'); ?>" + "</em></td></tr></table>" +
	  "<p style='margin-top: 60px'>" + "<? echo _('We also measured whether you judge similarities based on a single specific feature, or whether you judge similarities based on a set of broader, thematic features, as below:'); ?>" +
	  "<table><tr><td><b>" + "<? echo _('Specific feature'); ?>" + "</b></td><td></td><td><b>" + "<? echo _('Thematic features'); ?>" + "</b></td></tr><tr><td><img src='formal_intuitive_img/6-1.png' style='width: 235px' /></td><td><img src='images/flower_arrows.png' style='width: 185px' /></td><td><img src='formal_intuitive_img/6-2.png' style='width: 235px' /></td></tr><tr><td><em style='font-size: 12pt'>" + "<? echo _('In this set of images, the larger flower shares a specific feature (a curved stem) with all flowers in the group.'); ?>" + "</em></td><td></td><td><em style='font-size: 12pt'>" + "<? echo _('In this set of images, the larger flower shares several features (pointed petals, a leafless stem, an orange center) with three of the four flowers in the group, but does not share any one feature with all of the flowers in the group.'); ?>" + "</em></td></tr></table><br />" +
	  "<p><em>" + "<? echo _('[1]  Ji, L., Nisbett, R.E., & Zhang, Z. (2005).  Is it culture or is it language:  Examination of language effects in cross-cultural research on categorization.  Journal of Personality and Social Psychology, 87, 57-65'); ?>" + "</em></p>" +
	  "<p><em>" + "<? echo _('[2]  Norenzayan, A., Smith, E.E., Kim, B. J., & Nisbett, R.E. (2002).  Cultural preferences for formal versus intuitive reasoning.  Cognition Science, 26, 653-684.'); ?>" + "</em></p>" +
	  "<p id='footnote'><em>" + footnote + "</em></p>" +
	  "<button id='score_exp_less_button' class='btn exp_read_buttons'>" + "<? echo _('Show less') ?>" + "</button></div>" +
	  "<div id='feedback'><p>" + "<? echo _('Are you satisfied with the feedback you received and with the way we show your results? We are always open for suggestions, so please let us know what you think!'); ?>" + "</p>" +
	  "<textarea class='comments_box1' id='comment2'  name='comment2' onfocus='onFocusBox(this);' onblur='onBlurBox(this);'>" +
	  "</textarea>" +
	  "<input type='submit' value=<?php echo _('Submit'); ?> id='comment_submit' onclick='commentsubmit()'></div>");

  showFooter([<?= '"' . _("What is your website aesthetic?") . '"' ?>,
	  <?= '"' . _("How fast is your memory?") . '"' ?>]);

	// hide FULL explanation first, show only PREVIEW until "Read more" button is clicked
	$("#score_exp").hide();

	// attach listeners to track hover time on results page explanation
	var resultsExpHoverTimeTotal = 0;
	var resultsExpEnter = 0;
	var resultsExpVerifyMouseEnter = false;
	$("#score_exp").on("mouseenter", function () {
		resultsExpVerifyMouseEnter = true;
		resultsExpEnter = new Date().getTime();
	}).on("mouseout", function () {
		// guard against two mouse out events firing back to back (from rapid mouse movements) and corrupting resultsExpHoverTimeTotal
		if (resultsExpVerifyMouseEnter) {
			resultsExpHoverTimeTotal += new Date().getTime() - resultsExpEnter;
			resultsExpVerifyMouseEnter = false;
			resultsExpEnter = 0;
		}
	});
	var readMoreClicked = 0; // 0 = False, 1 = True
	var showLessClicked = 0; // 0 = False, 1 = True
//	var readMoreEnter = 0; // time click read more button
	showSlide("results");

	$('#score_exp_more_button').on("click", function () {
		readMoreClicked++; // increment how many times participant clicks "Read more" button
		// LISTEN for TIME spent in section (see above for some code/inspiration)
//		console.log(readMoreClicked);
		$("#score_exp_preview").hide();
		$("#score_exp").show(
			"slide", {direction: "up"}, 1000
		);
	});
//	$('#score_exp_more_button').click(function () {
//		readMoreClicked = 1;
//		// LISTEN for TIME spent in section (see above for some code/inspiration)
//		$("#score_exp_preview").hide();
//		$("#score_exp").show(
//			"slide", {direction: "up"}, 1000
//		);
//	});
	$('#score_exp_less_button').on("click", function () {
		showLessClicked++; //increment how many times participant clicks "Show less" button
		// USE to calculcate time spent in section?? (in conjunction with more clicking time??
//		console.log(showLessClicked);
		$("#score_exp").hide();
		$("#score_exp_preview").show(
			"slide", {direction: "down"}, 1000
		);
	});
//	$('#score_exp_less_button').click(function () {
//		showLessClicked = 1;
//		// USE to calculcate time spent in section?? (in conjunction with more clicking time??
//		$("#score_exp").hide();
//		$("#score_exp_preview").show(
//			"slide", {direction: "down"}, 1000
//		);
//	});




	// COLLECT & SEND DATA TO DB
	// window scrolling
	var win_height = $( window ).height();
	var win_width = $( window ).width();
	var is_scroll = 0;
	var max_scroll = 0;
	var scroll_max = 0.0;

	$(window).scroll(function() {
		var wintop = $(window).scrollTop(),
			docheight = $(document).height(),
			winheight = $(window).height();

		var  scrolltrigger = 0.95;
		if (wintop != 0) is_scroll = 1;
		var percent = (wintop/(docheight-winheight))*100;
		if (percent > scroll_max) scroll_max = percent.toFixed(2);
	});

	// Function that sends data to DB when closing browsers
	$(window).bind('beforeunload', function (e) {
//		var confirmationMessage = "Thanks for participating!";

		var leave = new Date().getTime();
		$.ajax({
			type: 'POST',
			url: 'includes/results_page_metrics.php',
			data: {
				index: 0, //used to be index: ind
				isscroll: is_scroll,
				maxscroll: scroll_max,
				height: win_height,
				width: win_width,
				entertime: enter,
				leavetime: leave,
				participant_id: vars.participant_id,
				graphHover: resultsHoverTimeTotal,
				condition: condition,
				participant_country: vars.participant_country,
				browser_lang: litw_locale,
				analytic_score: totalAnalytic,
				holistic_score: totalHolistic,
				read_more_click: readMoreClicked,
				show_less_click: showLessClicked,
				resultsExpHover: resultsExpHoverTimeTotal
			},
			async: false
		}).success(function (d) {
//			confirmationMessage = d;
			alert("Succcess!");
		}).fail(function (d) {
//			confirmationMessage = d;
			alert(d);
		});

//		e.returnValue = confirmationMessage;
//		return confirmationMessage;
	});

	function logData() {
		var leave = new Date().getTime();
		$.ajax({
			type: 'POST',
			url: 'includes/results_page_metrics.php',
			data: {
				index: 0, //used to be index: ind
				isscroll: is_scroll,
				maxscroll: scroll_max,
				height: win_height,
				width: win_width,
				entertime: enter,
				leavetime: leave,
				participant_id: vars.participant_id,
				graphHover: resultsHoverTimeTotal,
				condition: condition,
				participant_country: vars.participant_country,
				browser_lang: litw_locale,
				analytic_score: totalAnalytic,
				holistic_score: totalHolistic,
				read_more_click: readMoreClicked,
				show_less_click: showLessClicked,
				resultsExpHover: resultsExpHoverTimeTotal
			},
			async: false
		}).success(function (d) {
			console.log(d);
		}).fail(function (d) {
			console.log(d);
		})
	};

	// attach listeners for sharing buttons so we'll record clicks on the results page
	// "service" is the sharing service; "location" refers to which icon set was clicked: "header_share" refers to the header buttons; "results_share" refers to the results page body buttons
	$(".sharing_buttons img").each(function() {
		$(this).on("click", function() {
			var data = {
				participant_id: parseInt(vars.participant_id),
				service: $(this).attr("id"),
				location: $(this).closest("div").attr("id")
			}

			$.ajax({
				type: 'POST',
				url: 'includes/sharingClick.php',
				data: data
			});
		});
	});
};

  // GLOBAL variables that are particularly useful for rendering the results graph
  // countryMeans: an (alphabetized) array that stores country means,
  // allowing for one edit that changes the visuals as the means change
  var countryMeans = [

//	  Modes as of Nov. 22, 2016
      6, // WORLD
	  8, // Canada
	  4, // China
	  6, // Germany
	  8, // India
	  4, // Japan
	  2, // UK
	  6 // US

//  	Means as of Nov. 21, 2016
//      4.221, // WORLD
//	  4.315, // Canada
//	  3.932, // China
//	  2.515, // Germany
//	  4.802, // India
//	  2.538, // Japan
//	  3.531, // UK
//	  4.645 // US

//  	older averages
//	  3.76, //WORLD
//	  5.32, //Canada
//	  1.61, // China
//	  2.51, //Germany
//	  5.10581, //India
//	  2.33, //Japan
//	  2.93, //UK
//	  4.645 //US
//	  4.86852 // US older
//      7 //TEMP
  ];
//  var globalMean = 3.76;
  var MAX_SCORE = 24; // max number of questions/points participant can answer/earn in holistic and analytic (separately)
  var visualOffset = 12; // when graphed on X, Y Plane the X coordinates are graphed approx -7.25 points off, determined deductively by trial/error

// @param: holistic: participant's holistic score
// @param: analytic: participant's analytic score
// @param: area: handle of the area we graph in, part of the larger results graph svg
// @param width: width of the graph
// @param height: height of the graph
// @precondition: countryIndex must be initialized and assigned
function renderGraph(holistic, analytic, width, height, area) {
	// depending on condition, determine which comparison scores to graph
	var countryScore = [];
	var countryCoords = []; // coordinates of country average
    var countryCircleCoords = []; // coordinates of country circle

	// All participants see a country circle or a global circle
    countryScore.push(countryMeans[countryIndex]);

	calculateCoords(countryScore, countryCoords);
	calculateCoords(countryScore, countryCircleCoords); // because country circle is not centered on country's mean,
	 														// separately calculate coordinates of country circle to graph
	// [0] = scaling factor for/along the x-axis, [1] = scaling factor for/along the y-axis
	var scalingFunctions = setUpAxes(width, height, area);

	graphParticipantScore(holistic, analytic, scalingFunctions[0], scalingFunctions[1], area);
	graphCountryCircle(countryCircleCoords, 3, scalingFunctions[0], scalingFunctions[1], area); // hard-coded
	graphCountryAverage(countryCoords, scalingFunctions[0], scalingFunctions[1], area);
}

// HELPER FUNCTIONS

// func mainly included for determining x,y coordinates for country means; coordinates needed to plot in 2-dimensions
// @param: arrOfOverallScores = array containing scores, as calculated by Holistic - Analytical
// @param: arrofCoords = array that initially starts empty and then is filled at pos i with the x,y coordinates
//                      corresponding with arrOfOverallScores[i]
// @precondition: arrOfOverallScores is nonempty, arrOfCoords is empty
// @postcondition: arrOfOverallScores is unchanged, arrOfCoords is filled,
//                  arrOfOverallScores.length == arrOfCoords.length
function calculateCoords(arrOfOverallScores, arrOfCoords) {
    arrOfOverallScores.forEach(function (e, i, arr) {
	var holistic; // holistic score (h)
	var analytic; // analytic score (a)

	if (e >= 0) // more holistic or equally holistic and analytic
	{
        holistic = (MAX_SCORE + e) / 2; // based on system of equations: h + a = 24 and h - a = e
		analytic = MAX_SCORE - holistic;
	}
	else // more analytic
	{
		analytic = (MAX_SCORE - e) / 2;
		holistic = MAX_SCORE - analytic;
	}
	arrOfCoords.push([holistic + visualOffset, 24 - analytic]); // subtract from 18 because the y axis
																	  // crosses the x axis at 24 (flipped)
	});
}

  // determine radius of world/country circle to show to participants in the countryCircle
  // @return smallest radius that would encompass at least both the low and high points of the range to be covered
  function calculateRadius() {
	  var low = countryRanges[countryIndex][0];
	  var high = countryRanges[countryIndex][1];
	  var mean = countryMeans[countryIndex];

	  var lowCoords = [];
	  var highCoords = [];
	  var meanCoords = [];
	  calculateCoords([low], lowCoords);
	  calculateCoords([high], highCoords);
	  calculateCoords([mean], meanCoords);

	  var lowDist = calculateDistance(lowCoords[0], meanCoords[0]);
	  var highDist = calculateDistance(highCoords[0], meanCoords[0]);

	  if(condition == 0) // similar condition
	  {
		  //return larger radius that will encompass participant's score (similarity emphasized)
		  if (lowDist > highDist) {
			  return lowDist;
		  }
		  else // lowDist <= highDist
		  {
			  return highDist;
		  }
	  }
	  else // different condition
	  {
		  //return smaller radius so that participant's score is excluded in circle (difference emphasized)
		  if (lowDist < highDist) {
			  return lowDist;
		  }
		  else // lowDist >= highDist
		  {
			  return highDist;
		  }
	  }
  }

  function calculateDistance(a, b) {
	  return Math.sqrt(Math.pow(b[1] - a[1], 2) + Math.pow(b[0] - a[0], 2));
  }

// @param area: area of larger results graph svg to draw axes and graph
// @param width: width of the plane/x-axis
// @param height: height of the plane/y-axis
// @return array of axis scaling functions, where array[0] = x axis and array[1] = y axis
function setUpAxes(width, height, area) {
// define scales
	var x = d3.scaleLinear()
		.domain([0, MAX_SCORE])
		.range([0, width]); // hard-coded range of X and Y of coordinate plane
	var y = d3.scaleLinear()
		.domain([0, MAX_SCORE])
		.range([0, height]); // hard-coded range of X and Y

	// draw axes
	var axes = area.append("g");
	var xAxis = axes.append("g")
		.attr("transform", "translate(150," + height + ")") //translate so that x and y axis cross
		.attr("class", "axis")
		.call(d3.axisBottom(x).ticks(0)); // no tick marks
	var yAxis = axes.append("g")
		.attr("transform", "translate(150," + width + ")")
		.attr("class", "axis")
		.attr("transform", "translate(150, 0)") // translate so have space for y axis label
		.call(d3.axisLeft(y).ticks(0)); // do not show any tick marks

	// draw arrowheads
	var xAxisArrowhead = xAxis.append("path")
		.attr("d", "M2,2 L2,13 L8,7 L2,2")
//		.attr("marker-end", "url(#markerArrow)") // couldn't figure out how to work/use the markerArrow element
		.attr("transform", "translate(" + (width - 3) + ", -6.5)")
		.attr("fill", "black");
	var yAxisArrowhead = yAxis.append("path")
		.attr("d", "M2,2 L2,13 L8,7 L2,2")
//		.attr("marker-end", "url(#markerArrow)") // couldn't figure out how to work/use the markerArrow element
		.attr("transform", "translate(-7.75,4) rotate(-90)")
		.attr("fill", "black");

	// add labels to axes
	// group axis labels together in order to style them together uniformly
	var axisLabels = axes.append("g")
		.attr("class", "axis-label")
		.attr("text_anchor", "end");
	var xAxisLabel = axisLabels.append("g");
	xAxisLabel.append("text")
		.attr("x", width/2 + 10 ) // hardcoded values dependent on text length
		.attr("y", height + 20)
		.text("<? echo _('More holistic, intuitive thinking style') ?>");

	var yAxisLabel = axisLabels.append("g");
	yAxisLabel.append("text")
		.attr("x", -300)
		.attr("y", 140)
		.attr("transform", "rotate(-90)")
		.text("<? echo _('More analytic, rule-based thinking style') ?>");
	return [x, y];
}

// @param holistic: participant's holistic score
// @param analytic: participant' analytic score
function graphParticipantScore(holistic, analytic, xScale, yScale, area) {
	// NOTE: Unlike previous versions of this study, the variable userScore is not necessary
	// because we have separate holistic and analytic scores that we can directly use as x,y coordinates

	// subtract from 24 because the y axis crosses the x axis at 24/MAX_SCORE (flipped)
	var userCoords = [[totalHolistic + visualOffset, 24  -  totalAnalytic, "<? echo _('Your results'); ?>"]];
		//, [14 + visualOffset, 24 - 10, "4"], [16.5 + visualOffset, 24 - 7.5, "9"]];

	area.selectAll("dot")
		.data(userCoords)
		.enter().append("circle")
					.attr("r", 3.5) // 3.5 for dot (user), larger for country range
					.attr("fill", "black") // default is black (could exclude this line)
					.attr("cx", function (d) {
						return xScale(d[0]);
					})
					.attr("cy", function (d) {
						return yScale(d[1])
					});


	var spaceX = 5; // space between data point and label
	var spaceY = 5;
	var labels = area.append("g");
	var text = labels.selectAll("text")
		.data(userCoords)
		.enter().append("text")
		.attr("class", "point-label")
		.attr("id", "participant_point")
		.attr("transform", function (d) {
			return "translate(" + (xScale(d[0]) + spaceX) + "," + (yScale(d[1]) + spaceY) + ")";})
		.text(function (d) {
			return d[2];
		});
  }

// @param coords: Holistic, Analytic (X,Y) coordinates of the center of the country circle to render
// @param radius: radius of country circle
// @param area: handle of area (part of larger results graph svg) to render country circle in
function graphCountryCircle(coords, radius, xScale, yScale, area) {
	area.selectAll("dot")
		.data(coords)
		.enter().append("circle")
		.attr("r", xScale(radius)) // 3.5 for dot (user), larger for country range
		.attr("fill", "url(#circles-3)")
		.attr("opacity", 0.2)
		.attr("cx", function (d) {
            return xScale(d[0]);
		})
		.attr("cy", function (d) {
            return yScale(d[1])
		});

	if(countryIndex != 0) // country comparison possible (& browser in English)
	{
		coords[0].push("<? echo _('How other participants from'); ?>" + " " + vars.participant_country + " " + "<? echo _('usually score'); ?>"); // coords[0] is a 2-elt array [x,y], add a third value in order to label circle
	}
	else // country comparison not possible, global comparison shown instead
	{
		coords[0].push("<? echo _('Participants from all around the world'); ?>"); // coords[0] is a 2-elt array [x,y], add a third value in order to label circle
//		coords[0].push("<?// echo _('usually score in this circle'); ?>//"); // coords[0] is a 2-elt array [x,y], add a third value in order to label circle
//		console.log(coords[0]);
	}
	var spaceX = -50; // space between data point and label
	var spaceY = 25;
	var labels = area.append("g");
	var text = labels.selectAll("text")
		.data(coords)
		.enter().append("text")
		.attr("class", "point-label")
		.attr("transform", function (d) {
			return "translate(" + (xScale(d[0]) + spaceX) + "," + (yScale(d[1]) + yScale(radius) + spaceY) + ")";}) // use radius to have label outside of circle
		.text(function (d) {
			return d[2];
		});
//	labels.selectAll("text")
//		.data(coords)
//		.enter().append("text")
//		.attr("class", "point-label")
//		.attr("transform", function (d) {
//			return "translate(" + (xScale(d[0]) + spaceX) + "," + (yScale(d[1]) + yScale(radius) + spaceY) + ")";}) // use radius to have label outside of circle
//		.text(function (d) {
//			return d[3];
//		});

}

// @param coords: Holistic, Analytic (X,Y) coordinates of the center of the country average to render
// @param area: handle of area (part of larger results graph svg) to render country circle in
function graphCountryAverage(coords, xScale, yScale, area) {
	area.selectAll("dot")
		.data(coords)
		.enter().append("circle")
		.attr("r", 3.5)
		.attr("fill", "green")
		.attr("opacity", 0.4)
		.attr("cx", function (d) {
			return xScale(d[0]);
		})
		.attr("cy", function (d) {
			return yScale(d[1]);
		});

	if(countryIndex != 0) // country comparison possible (& browser in English)
	{
		coords[0].push(vars.participant_country + " " + "<? echo _('average'); ?>"); // coords[0] is a 2-elt array [x,y], add a third value in order to label circle
	}
	else // country comparison not possible, global comparison shown instead
	{
//		coords[0].push("Participants from all around the world"); // coords[0] is a 2-elt array [x,y], add a third value in order to label circle
//		return "<?// echo _('Participants from all around the world'); ?>//";
		coords[0].push("<? echo _('World average'); ?>"); // coords[0] is a 2-elt array [x,y], add a third value in order to label circle
	}
	var spaceX = 25; // space between data point and label
	var spaceY = 3;
	var labels = area.append("g");
	var text = labels.selectAll("text")
		.data(coords)
		.enter().append("text")
		.attr("class", "point-label")
		.attr("fill", "green")
		.attr("transform", function (d) {
			return "translate(" + (xScale(d[0] + 3) + spaceX) + "," + (yScale(d[1]) + spaceY) + ")";}) // use radius to have label outside of circle
		.text(function (d) {
			return d[2];
		});
}

  // @param holistic: total holistic score for participant
  // @param analytic: total analytic score for participant
  // @return jquery string that gives an explanation for/assessment of participant's scores
  function getParticipantAssessment(holistic, analytic) {
	  // determine assessment text for participant's score
	  // precondition: totalHolistic and totalAnalytic are always, guaranteed to be integers
	  if (holistic <= 4) // totalAnalytic >= 20
	  {
		  return "<? echo _('Based on your results, your thinking style appears to be <em>strongly intuitive.</em> When tackling a problem, you may tend to look at the bigger picture and examine functional relationships.'); ?>";
	  }
	  else if (holistic > 4 && holistic <= 9) // 15 <= totalAnalytic <= 19
	  {
		  return "<? echo _('Based on your results, your thinking style appears to be <em>somewhat intuitive.</em> When tackling a problem, you may tend to look at the bigger picture and examine functional relationships.'); ?>";
	  }
	  else if (holistic > 9 && holistic <= 14) // 10 <= totalAnalytic < 15, 10 <= totalHolistic < 15
	  {
		  return "<? echo _('Based on your results, your thinking style appears to be <em>both intuitive and analytic.</em> When tackling a problem, you may tend to focus both on the larger picture and on specific details.'); ?>";
	  }
	  else if (holistic  > 14 && holistic <= 19) // 15 <= totalHolistic <= 19
	  {
		  return "<? echo _('Based on your results, your thinking style appears to be <em>somewhat analytic.</em> When tackling a problem, you may tend to focus on details as opposed to the big picture.'); ?>";
	  }
	  else // totalHolistic >= 20
	  {
		  return "<? echo _('Based on your results, your thinking style appears to be <em>strongly analytic.</em> When tackling a problem, you may tend to focus on details as opposed to the big picture.'); ?>";
	  }
  }

  // @param globalShown: true if comparison group is the world, false if comparison group is participant's country
  // @return jquery string that compares participant's score with their country or the world, depending on condition
  function getComparisonAssessment(condition) {
      //countryIndex == 0 means global shown
      //countryIndex != 0 means country shown
	  if (countryIndex === 0) {
          if (condition === 0) {
//              return "<?// echo _('You think in a way that is <em><strong>similar</strong></em> to how people from all around the world tend to think.'); ?>//"
          		return "<? echo _('You think like a lot of other people in the world!'); ?>"
          }
          else {
//              return "<?// echo _('You think in a way that is <em><strong>different</strong></em> from how people from all around the world tend to think.'); ?>//"
			  	return "<? echo _('Your thinking style is <em><strong>unique</strong></em>!'); ?>"
		  }
      }
      else {
          if (condition === 0) {
              return "<? echo _('You think in a way that is <em><strong>similar</strong></em> to how people from your country tend to think.'); ?>"
          }
          else {
              return "<? echo _('You think in a way that is <em><strong>different</strong></em> from how people from your country tend to think.'); ?>"
          }
      }
  }


  //Yan add this
  function commentsubmit() {
	  // process the responses
	  var comment2 = ($$$("comment2").value == <? echo _("'You may leave this area blank.'"); ?>) ? "" : $$$("comment2").value;

	  $("#comment2").val("Thank you!").attr("disabled", true);
	  $("#comment_submit").css("visibility", "hidden");
	  // AJAX request
	  $.ajax({
		  type: 'POST',
		  url: 'includes/results_page_comments.php',
		  data: {
			  results_page_comments: comment2,
			  participant_id: vars.participant_id
		  }
	  });
  }

  function recordCountry() {
	  $.ajax({
		  type: "POST",
		  url: "includes/recordCountry.php",
		  data: { locale: litw_locale }
	  }).done(function(data) {
//		  console.log(data);
		  data = JSON.parse(data);
		  vars.participant_id = data["participant_id"];
		  vars.participant_country = data["ip_country"];
		  browser_lang = data["locale"];
		  setup();
	  }).fail(function (message) {
		console.log(message);
	  });
  }

window.onload = recordCountry;
//window.onload = function() {
//	insertHeader();
//	$("#header").show();
//	finish();
//}