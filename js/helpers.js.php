/*
 * helpers.js
 *
 * Helper functions
 *
 */
function createButton(text,field, func){
	 setHTML(field,"<button onclick = \""+func+"\" >"+text+" </button>");
} 

/* shorthand for call to set innerHTML */
function setHTML(id, text) {
	document.getElementById(id).innerHTML = text;
}

function getHTML(id) {
	return document.getElementById(id).innerHTML;
}

function getElem(id) {
	return document.getElementById(id);
}

function setCanvas(x, y) {
	ctx.canvas.width = x;
	ctx.canvas.height = y;
}

function clearHTML()
{
	setHTML("directions","");
}

/* based on user number, counterbalance the trials
 * one less than the number of trials counterbalances
 * counterbalance by slicing off the front of the arrays
 * and adding to the back of the array */
function ctrbalance() {
	var counterbalance = user % fsize.length;
	var numtrials = fsize.length;
	var first = fsize.slice(0, counterbalance);
	var second = ssize.slice(0, counterbalance);
	var length = lsize.slice(0, counterbalance);
	fsize = fsize.slice(counterbalance);
	fsize = fsize.concat(first);
	ssize = ssize.slice(counterbalance);
	ssize = ssize.concat(second);
	lsize = lsize.slice(counterbalance);
	lsize = lsize.concat(length);
}

/* Since drawing exactly on an integer results in blurriness (http://diveintohtml5.info/canvas.html),
 * Boost pixel number up 0.5 if on an integer */
function pixadjust(value) {
	if (value%1 == 0) { return value+0.5 }
	return value;
}

//Browser Support Code, from http://www.tizag.com/ajaxTutorial/ajaxbrowsersupport.php
function ajaxFunction()
{
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
}