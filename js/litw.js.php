<?php require("../localization.php"); ?>

var commonResLoc = (typeof commonResourcesLoc) == "undefined" ? "../common/" : commonResourcesLoc;

function getCommonImageLinksArray() {
	return [commonResLoc + "img/ajax-loader.gif", commonResLoc + "img/fail.png", commonResLoc + "img/logo.png", commonResLoc + "img/next.png", commonResLoc + "img/next_active.png"];
}

function getHeaderWithoutShareHTML() {
	return "<a href='http://www.labinthewild.org'><img src='" + commonResLoc + "img/logo.png' alt='logo' id='logo'/></a><div class='separator'> &nbsp; </div>";
}

function getLikertScaleHTML(left_text, right_text) {
	return '<table id="likert_scale"><tr>' +
	'<td style="text-align: center; width: 75px;">' + left_text + '</td>' +
	'<td style="padding-left: 30px; padding-right: 30px; width:80%;">' +
	'<form action="" style="text-align: center;">' +
		'<input type="radio" name="scale_value" value="1" />' +
		'<input type="radio" name="scale_value" value="2" />' +
		'<input type="radio" name="scale_value" value="3" />' +
		'<input type="radio" name="scale_value" value="4" />' +
		'<input type="radio" name="scale_value" value="5" />' +
		'<input type="radio" name="scale_value" value="6" />' +
		'<input type="radio" name="scale_value" value="7" />' +
		'<input type="radio" name="scale_value" value="8" />' +
		'<input type="radio" name="scale_value" value="9" />' +
   '</form></td>' +
   '<td style="text-align: center; width: 75px;">' + right_text + '</td>' +
   '</tr></table>';
}

function getAJAXWorkingHTML() {
	return '<br><img src="' + commonResLoc + 'img/ajax-loader.gif" alt="ajax loader" style="vertical-align: middle;"/> Please wait one moment...';
}

function getFatalErrorHTML() {
	return "<img src='" + commonResLoc + "img/fail.png' alt='frowny' style='display:block;margin:0 auto;'/><br/>" +
		"<h2>A Fatal Error Occurred!</h2><br/>" +
		"<p>Lab in the Wild has been notified of the problem, and a team of highly trained whales are on it. We apologize for the inconvenience.</p>" +
		"<p>Need to vent? (Or feeling especially helpful?) Email us at <a href='mailto:info@labinthewild.org'>info@labinthewild.org</a> to tell us how this happened and how it made you feel.</p>" +
		"<p style='text-align:center;'><br/><a href='index.php'>Try this test again</a><br/><br/><a href='http://www.labinthewild.org'>Return to Lab in the Wild</a></p>";	
}

function getBrowserErrorHTML(error) {
	return "<img src='" + commonResLoc + "img/fail.png' alt='frowny' style='display:block;margin:0 auto;'/><br/>" +
		"<h2>Your browser or device is not compatible!</h2><br/>" +
		"<p>" + error + "</p>" +
		"<p>Feel like we made a mistake and that you should be able to take this test with the device or browser you are using? Email us at <a href='mailto:info@labinthewild.org'>info@labinthewild.org</a>! </p>" +
		"<p style='text-align:center;'><br/><a href='http://www.labinthewild.org'>Return to Lab in the Wild</a></p>";
}

/*
 * Handles test errors arising from bad AJAX requests (e.g., when demographics and/or
 * data info via the server goes kaput) by attempting another (hopefully successful)
 * AJAX request with debugging info and persuading the participant to e-mail us, too.
 *
 * Parameter: err, a string of the original error to send through the AJAX request
 * Returns:   nothing
 * Questions? E-mail dhu@college.harvard.edu || artemis1593@gmail.com
 */
function handleError(err) {
    // create showFailPage (and accompanying error div) if necessary
    if (typeof showFailPage !== "function") {
        $('<div id="error"></div>').appendTo("body");
        $$$("error").innerHTML = getFatalErrorHTML();

        function showFailPage() {
            // assume we have content div
            $$$("content").style.display = "none";
            window.onbeforeunload = null;
            $$$("error").style.display = "block";
            throw new Error('Fatal Error');
        }
    }

    // data to send via ajax
    var data = {
        participant_id: vars.participant_id,
        test_url: window.location.toString(),
        user_agent: navigator.userAgent,
        ajax0: err,
        ajax1: "",
        database_id: -1
    };

    // try to send data via ajax request, and update data based on success
    $.ajax({
        type: 'POST',
        url: commonResLoc + 'include/testerror.php',
        data: data
    }).done(function (response) {
                try {
                    response = $.parseJSON(response);
                    data.ajax1 = response.ajax1;
                    data.database_id = response.database_id;
                }
                catch (e) {
                    data.ajax1 += "\n" + e.toString();
                }
            })
      .fail(function ()   { data.ajax1 = "Failed!"; })
      .always(function () { failAwesomely(); });

    // now that we have standard showFailPage, amp it up to display more info
    function failAwesomely () {
        var msg = 
            <?=
                '"' .
                    _("<p><b>Be our hero!</b> If you e-mail us, it would be " .
                      "awesomely helpful if you could copy and paste to your " .
                      "message the information in curly braces below:</p>") .
                '"'
            ?> +
            "<p style='text-align:left; width:75%; margin-left:10%;'>" +
                JSON.stringify(data, null, " ") +
            "</p>" +
            <?=
                '"' .
                    _("<p>Also let us know if we can get in touch with you to " .
                      "troubleshoot the error or let you know when we release a " .
                      "fix! In the meantime...</p>") .
                '"'
            ?>;

        $(msg).insertBefore("#error p:last-child");
        showFailPage();
    }
}

/*
 * To the bottom of the results page, adds cool sharing buttons, links to other
 * tests, and the copyright thing. To use this, make sure that the test's index.php
 * already has the following HTML somewhere after the results div:
    <div id="results2">
        <div id="end_share">
            <span class='st_facebook_large' displayText='Facebook'></span>
            <span class='st_twitter_large' displayText='Tweet'></span>
            <span class='st_pinterest_large' displayText='Pinterest'></span>
            <span class='st_email_large' displayText='Email'></span>
        </div>
    </div>
 * Parameter: testTitles, a string array of the titles of tests you want to link to
 *            MAKE SURE THIS IS GETTEXTED!
 * Returns:   nothing
 * Questions? E-mail dhu@college.harvard.edu || artemis1593@gmail.com
 */
function showFooter(testTitles) {
    // insert preliminary footer before we get to sharing buttons
	var begin_share =
		"<div class='separator' style='margin-top: 50px; margin-bottom: 0px; height:5px; clear:left;'> &nbsp; </div>" +
		"<p style='text-align:center; font-size:18px; margin-top: 50px;'>" +
		"<? echo _('<span class=\'bolded-blue\'>Thanks again!</span> Thought that was fun?'); ?>" + "</p>" +
		"<div style='font-size:18px; margin-bottom:5px;'>" + "<? echo _('Tell your friends:'); ?>" + "</div>";
	
	$(begin_share).insertBefore("#end_share");
    
    // now begin links to other stuff
	var links =
		"<br/><p style='font-size:18px; margin-bottom:5px;'>" +
        "<?php echo _('And consider taking some of our other tests:'); ?>" + "</p>" +
		"<table id='tests-table'>";

	// json info of other tests to link to
    var tests = $.parseJSON(
        <?php
            require_once($_SERVER["DOCUMENT_ROOT"]."/include/test_grid_data.php");

            // the JSON_HEX_APOS flag here deals with single apostrophes that appear in translations. It converts them to their Unicode escape sequence.
            echo "'" . json_encode($data, JSON_HEX_APOS) . "'";
        ?>
    );
    
    // add test info to table for tests we want to display
    $.each(tests, function (index, t) {
        // if title is in our array of desired test titles
        if ($.inArray(t.title, testTitles) !== -1) {
            // correct img url (absolute)
            t.img = "http://labinthewild.org/"+t.img;
        
            links += 
                "<tr>" +
                    "<td>" +
                        "<a href='../../" + t.url + "'><img src='" + t.img + "' /></a>" +
                    "</td>" +
                    "<td>" + 
                        "<p><a href='../../" + t.url + "'>" + t.title + "</a></p>" +
                        "<p>" + t.desc +
                            "<a href='../../" + t.url + "'> " +
                                <?= "\"".$participate."\"" ?> +
                            "</a>" +
                        "</p>" +
                    "</td>" +
                "</tr>";
        }
    });
    
    // finish off the table and officially append to results
    links += "</table>";

    $(links).appendTo("#results");
    
    // link back to home page and put copyright at bottom
    var home =
		"<p style='font-size:18px; text-align:center;'>" +
			"<? echo _('Or <a href=\'http://www.labinthewild.org/\'>Return to the Wild</a>!'); ?>" +
		"</p>" +
        "<div class='separator' style='margin-top:50px; margin-bottom:0px; height:5px; clear:left;'> &nbsp; </div><div style='display: block; width: 100%; text-align: center; margin-bottom: 40px; color:#777; font-size: 12px;'>" + "<? echo _('Copyright 2016, LabintheWild.'); ?>" + "</div>";	
	$(home).appendTo("#results");

	// show the whole dang thang
	$$$("results").style.display = "block";
}