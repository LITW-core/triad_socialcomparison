/*************************************************************
 * demographics.js.php
 * 
 * Sets up the demographics form and performs the necessary
 * checks before the subject is allowed to move on.
 * 
 * Last Modified: February 4, 2015 by Trevor Croxson
 *
 * Author: Yuechen Zhao, Jun Sup Lee, Norman Zhu,
 * 		   adapted from a version created by Nancy Chen
 *
 * © Copyright 2015 LabintheWild
 * For questions about this file and permission to use
 * the code, contact us at info@labinthewild.org
 *************************************************************/
 <?php 
 	include ("../localization.php");
 	include ("../../cookies/input.js.php");
 	include ("../../cookies/data_en.js.php");
 	include ("../../demographics/questions.php");
?>

/* dictionary and arrays so that additional fields for countries lived in
 * can be added and cleared with the same code */



var multikey = {'country':0,'lang':1,'years':0,'fluency':1};
var multi1 = ['country', 'lang'];
var multi2 = ['years', 'fluency'];

// options and counters


/* 
the correct country array (as per language) along with the english array will be needed for the 
cookies to work correctly in all languages this array below is just in english and hence the 
cookies work in the english version only
*/

var noyes = { <?php echo "' ':' '"
				. ", 'No' : " . _("'No'") 
				. ", 'Yes' : " . _("'Yes'")
			?> }; 

// var noyes = ['', 'No', 'Yes'];
var numctries = 0;

var languages = {
 ' ' : ' ', 
 'Persian': ' فارسی', 
 'Indonesian': 'Bahasa Indonesia', 
 'Catalan': 'Català', 
 'Czech': 'Čeština', 
 'Danish': 'Dansk', 
 'German': 'Deutsch', 
 'English': 'English', 
 'Spanish': 'Español', 
 'French': 'Français', 
 'Croatian': 'Hrvatski', 
 'Italian': 'Italiano', 
 'Lithuanian': 'Lietuvių', 
 'Hungarian': 'Magyar', 
 'Dutch': 'Nederlands', 
 'Norwegian': 'Norsk', 
 'Polish': 'Polski', 
 'Portuguese': 'Português', 
 'Romanian': 'Română', 
 'Slovak': 'Slovenčina', 
 'Slovenian': 'Slovenščina', 
 'Finnish': 'Suomi', 
 'Swedish': 'Svensk', 
 'Vietnamese': 'Tiếng Việt', 
 'Turkish': 'Türkçe', 
 'Greek': 'Ελληνικά', 
 'Bulgarian': 'български език', 
 'Russian': 'Русский', 
 'Serbian': 'Српски', 
 'Ukrainian': 'Українська', 
 'Hebrew': 'עברית', 
 'Arabic': 'اللغة العربية', 
 'Standard Hindi': 'हिन्दी', 
 'Thai': 'ภาษาไทย', 
 'Korean': '한국어', 
 'Chinese': '中文', 
 'Japanese': '日本語',
 'Other': 'Other'}; 

var numlangs = 0;

var years = [' ',0.5];
var web_usage = [' ',"< 1",1,2,3,4,5,6,7,8,9,"10+"];
<?php echo // urban array
	"var urban = {' ':' ',"
	. "'urban' : " . _("'urban'") 
	.",'suburban' : " . _("'suburban'") 
	.",'rural' : " . _("'rural'") ."};"  
?>

var age = [' '];
<?php echo // profession array
	"var profession = {' ' : ' ',"
	. "'Administrative' : " . _("'Administrative'") 
	. ", 'Architect' : " . _("'Architect'") 
	. ", 'Artist/Creative/Performer' : " . _("'Artist/Creative/Performer'") 
	. ", 'Banking/Financial' : " . _("'Banking/Financial'") 
	. ", 'Computers/Technology' : " . _("'Computers/Technology'") 
	. ", 'Craftsman/Construction' : " . _("'Craftsman/Construction'") 
	. ", 'Education' : " . _("'Education'") 
	. ", 'Engineer' : " . _("'Engineer'") 
	. ", 'Executive Management' : " . _("'Executive Management'") 
	. ", 'Food Services' : " . _("'Food Services'") 
	. ", 'Homemaker' : " . _("'Homemaker'") 
	. ", 'Legal' : " . _("'Legal'") 
	. ", 'Medical' : " . _("'Medical'") 
	. ", 'Military/Government/Politics' : " . _("'Military/Government/Politics'") 
	. ", 'Professional Trade' : " . _("'Professional Trade'") 
	. ", 'Real Estate' : " . _("'Real Estate'") 
	. ", 'Research' : " . _("'Research'") 
	. ", 'Retail' : " . _("'Retail'") 
	. ", 'Retired' : " . _("'Retired'") 
	. ", 'Sales/Marketing' : " . _("'Sales/Marketing'") 
	. ", 'Self-Employed' : " . _("'Self-Employed'") 
	. ", 'Student' : " . _("'Student'") 
	. ", 'Travel/Hospitality' : " . _("'Travel/Hospitality'") 
	. ", 'Unemployed' : " . _("'Unemployed'") 
	. ", 'Other' : " . _("'Other'") . "};" 
?>

<?php echo // fluency array
	"var fluency = { ' ' : ' ',"
	. "'fluently' : " . _("'fluently'") 
	. ", 'very well' : " . _("'very well'") 
	. ", 'well' : " . _("'well'") 
	. ", 'at beginner\'s level' : " . _("'at beginner\'s level'") . "};" 
?>


// =======================================================================
// starting here, I've swapped PHP variables with the gettext equivalents:
// =======================================================================


<?php echo // education array
	"var education = {' ' : ' '"
	. ",'pre-high school' : " . _("'pre-high school'")
	. ",'high school' : " . _("'high school'")
	. ",'college' : " . _("'college'")
	. ",'graduate school' : " . _("'graduate school'")
	. ",'professional school' : " . _("'professional school'")
	. ",'PhD' : " . _("'PhD'")
	. ",'postdoctoral' : " . _("'postdoctoral'") . "};"  
?>

<?php echo // gender array
	"var gender = {' ' : ' '"
	. ", 'male' : " . _("'male'")
	. ", 'female' : " . _("'female'")
	. ", 'N/A' : " . _("'N/A'") . "};"
?>

for(var i = 6;i<100;i++){
	age.push(i);
}
for(var i=1; i<100; i++){
	years.push(i);
}

// requried form fields (for displaying red asterisk):
var def = ["retake","gender","age","multinational","home","homeyears","lang0","country","current","country1"];
// separate array used for validation at end:
var required_fields = ["retake","gender","age","multinational","country0","years0","lang0","country1","years1","country2","years2","country3","years3","country4","years4","country5","years5","country6","years6"];


/* the questions in order
 * Each question dictionary contains first part of sentence (header), end of sentence (footer)
 * and options in dropdown box (options)
 */
var questions = new Array();
	questions['retake'] = {'q':<?php echo _("'Have you taken this test before?'"); ?>,'options':noyes, 'translated' : true};
	questions['gender'] = {'q':<?php echo _("'What is your gender?'"); ?>};
	questions['gender']['options'] = gender;
	questions['gender']['translated'] = true; 
	// ************************************************************************
	questions['age'] = {'q':<?php echo _("'How old are you?'"); ?>,'options':age, 'translated' : false};
	// ************************************************************************
	questions['multinational'] = {'q':<?php echo _("'Have you lived in more than one country?'"); ?>,'options':noyes, "translated" : true};
	questions['home'] = {'q':<?php echo _("'In which country were you born?'"); ?>,'options':countries, 'translated' : true};
	questions['homeyears'] = {'q':<?php echo _("'How many years did you live there?'"); ?>,'options':years, 'translated' : false};
	questions['country1'] = {'q':'<i>' + <?php echo _("'Please add countries in order from the country you were born in to the most recent.'"); ?> + '</i><br><br>' + (($.inArray("country1", def) !== -1) ? "<span style='color:#9357F2;'>* </span>" : "") + <?php echo _("'First Country'"); ?>,'options':countries, 'translated' : true};
	questions['years1'] = {'q':'     ' + <?php echo _("'Year(s):'"); ?>,'options':years, 'translated' : false};
	questions['country'] = {'q':<?php echo _("'Next country:'"); ?>,'options':countries, 'translated' : true};
	questions['years'] = {'q':'     ' + <?php echo _("'Year(s):'"); ?>,'options':years, 'translated' : false};
	questions['current'] = {'q':<?php echo _("'Current country:'"); ?>,'options':countries, 'translated' : true};
	questions['currentyears'] = {'q':'     ' + <?php echo _("'Current Years:'"); ?>,'options':years, 'translated' : false};
	// ************************************************************************
	// questions['urban']= {'q':'Currently, I live in a mostly', 'options':urban, 'translated' : true};
	// ************************************************************************
	questions['lang0'] = {'q':<?php echo _("'What is your native language?'"); ?>,'options':languages, 'translated' : true};
	questions['lang'] = {'q':<?php echo _("'Language:'"); ?>,'options':languages, 'translated' : true};
	questions['fluency'] = {'q':'     ' + <?php echo _("'Fluency:'"); ?>, 'translated' : true};
	questions['fluency']['options'] = fluency;
	questions['father'] = {'q':<?php echo _("'What is your father\'s nationality?'"); ?>,'options':countries, 'translated' : true};
	questions['mother'] = {'q':<?php echo _("'What is your mother\'s nationality?'"); ?>,'options':countries, 'translated' : true};
	questions['education'] = {'q':<?php echo _("'What is the highest level of education you have received or are pursuing?'"); ?>, 'translated' : true};
	questions['education']['options'] = education; 
	// ************************************************************************
	// questions['web_usage']= {'q':'On a normal day, I am on the Internet for approximately','options':web_usage, 'translated' : true};
	// ************************************************************************
	// questions['profession']= {'q':'My profession is','options':profession, 'translated' : true};
	// ************************************************************************
	//questions['normalvision'] = {'q':<?php echo _("'Do you have normal or corrected-to-normal vision (including normal color vision)?'"); ?>,'options':noyes, 'translated' : true};

	
function whyAlert() {
	alert(<?php echo _("'We need this information for data analysis. Please note that none of the answers are personally identifiable. LabintheWild takes your privacy very seriously. You may email us at info@labinthewild.org for more information.'"); ?> ); 
}

/* called in studyinfo(), display of demographics form */
function demographics(elt_id, opts) {

	var elt = $("#" + elt_id)
	
	// begin form
	var html = "<br/><h4 class='bolded-blue' style='margin-bottom:30px;'>" + <?php echo _("'While we\'re calculating your results, please tell us a little bit about yourself.'"); ?> + 
	"<a href='#' onclick='whyAlert();'>" + <?php echo _("' Why?'"); ?> + "</a></h4><p style='color:#9357F2'>" + <?php echo _("'Please answer at least those questions marked with an asterisk (*).'"); ?> + "</p><br /><form name='demo' id='demo'>";

	// fill in all the questions
	for (title in questions) {
	
		// do not include question in "have" case of living in multiple countries or for additional language fields
		multinational = title == "country1" || title == "years1" || title == "country" || title == "years" || title == "current" || title == "currentyears";
		multilingual = title == "lang" || title == "fluency";
		if (!multinational && !multilingual) {
			
			/* add a div tag surrounding history of countries lived in */
			if (title == "home") { html += "<div id='history'>"; }
			
			// add full question
			html += dropdown(title);
			
			// place "Add Languages" button
			if (title == 'lang0') { html += " <a href = '#null' id='addlang'>" + <?php echo _("'[Add Another Language]'"); ?> + "</a>"; }
			
			// do not put a new line if question has multiple parts
			// var multiquestion = title == 'home'|| title=="gender";
			/* if (!multiquestion) { */ html += "<br><br>"; //}
			
			// add a div tag surrounding languages
			if (title == "lang0") {
				html += "<div id='lang'>";
				// add div tag for each additional language
				for (var i = 1; i < 7; i++) {
					html += "<span id='lang"+i+"'></span>";
				}
				html += "<a href = '#null' onclick='rmvlang()' id='delete_lang'></a>"
				html += "<div id='lang7'></div>";
				html += "</div>";
			}
			
			/* close div tag */
			if (title == "homeyears") { html += "</div>" }
		}

	}
	
	// end form
	html += "</form>";

	html += "<div><input type='checkbox' name='cookies' id='cookies' />" + <?php echo _("' I am on a personal computer.'"); ?> + "<a href='#null' id='whyCookie'>" + <?php echo _("' Why?'"); ?> + "</a></div>"; 
	setHTML("demographics", html);
	document.getElementById('whyCookie').onclick = function() {alert(<?php echo _("'If you are on a personal computer, we will store your demographics information on your local machine. This will allow you to skip the demographics form on any other Lab in the Wild tests that you might take. If you\'d prefer not to have a saved copy, simply leave the box unchecked.'"); ?>)}; 
	
	//	setHTML("demographics", html);
//	elt.html(html);
//    setHTML("demographics", html); 
//	elt.show(); 
	document.getElementById('addlang').onclick = addlang;

	inputFromCookie();
}

// add question and dropdown boxes for additional languages
function addlang() {
	createCookie("addlang", "1", 1);
	if (numlangs == 0) {
		numlangs++;
		var html = dropdown('lang', numlangs)+dropdown('fluency', numlangs);
		setHTML('delete_lang', <?php echo _("'[Delete]'"); ?> +  "<br><br> ");
		setHTML('lang'+numlangs, html);
	}
	else if (numlangs < 6) {
		numlangs++;
		var html = "<br><br>"+dropdown('lang', numlangs)+dropdown('fluency', numlangs);
		setHTML('lang'+numlangs, html);
	}
	else {
		setHTML('lang7',"");
		setHTML('addlang',"");
	}
}

function rmvlang() {
	createCookie("addlang", "0", 1);
	setHTML('lang'+numlangs,"");
	setHTML('lang7',"");
	setHTML('addlang',"<a href = '#null' id='addlang'>" + <?php echo _("'[Add Another Language]'"); ?> + "</a>");
	numlangs--;
	if (numlangs==0){
		setHTML('delete_lang',"");
	}
	
}

// add question and dropdown boxes for additional countries
function addctry() {
	createCookie("addCountry", "1", 1);
	if (numctries == 1) {
		numctries++;
		var html = dropdown('country', numctries)+dropdown('years', numctries);
		setHTML('delete_ctry', <?php echo _("'[Delete]'"); ?> + " <br><br> ");
		setHTML('country'+numctries, html);
	}
		
	else if (numctries < 5) {
		numctries++;
		var html = "<br><br>"+dropdown('country', numctries)+dropdown('years', numctries);
		setHTML('country'+numctries, html);
	}
	
	else {
		setHTML('country6',"");
		setHTML('addctry',"");
	}
}

function rmvctry() {
	createCookie("addCountry", "1", 1);
	setHTML('country'+numctries,"");
	setHTML('addctry',"<a href = '#null' id='addlang'>" + <?php echo _("'[Add Another Country]'"); ?> + "</a>");
	numctries--;
	if (numctries==1){
		setHTML('delete_ctry',"");
	}

}

// switch from one country lived in to multiple countries lived in
function history(input) {
	var html;
	
	// "yes" case
	if (input == 1) {
		numctries = 1;
		
		// "grew up in"
		html = dropdown('country1')+dropdown('years1');
		html += " <a href='#null' id='addctry'>" + <?php echo _("'[Add Another Country]'"); ?> + "</a><br><br>";
		
		// add div fields for each additonal country
		for (var i = 1; i <= 6; i++) {
			html += "<span id='country"+i+"'></span>";
		}
		html += "<a href = '#null' onclick='rmvctry()' id='delete_ctry'></a>"

		html += dropdown('current')+dropdown('currentyears')+"<br><br>";
	}
	
	// "no" case
	else if (input == 0) {
		numctries = 0;
		html = dropdown('home')+ "<br><br>" + dropdown('homeyears')+"<br><br>"; 
	}
	setHTML('history',html);
	
	if (input == 1) {
		document.getElementById('addctry').onclick = addctry;
	}
}

// set second field to blank if first field is blank
function blank(number, type) {
	index = document.demo[multi1[type]+number].options.selectedIndex;
	selected = document.demo[multi1[type]+number].options[index].value;
	if (selected == 0) {
		document.demo[multi2[type]+number].options.selectedIndex = 0;
	}
}

// clear both fields of question if "clear" button pressed
function clrentries(number, type) {
	document.demo[multi1[type]+number].options.selectedIndex = 0;
	document.demo[multi2[type]+number].options.selectedIndex = 0;
}

// fill in <select> and <option> fields

function dropdown (title, num) {
	var html = "";
	question = questions[title];
	
	// code to mark required questions in purple
	if ($.inArray(title, def) !== -1 && title != "country1") {
		html += "<span style='color:#9357F2;'>* </span>" + "<span>" + question['q']+ "</span>" + " ";
	} else {
		html += "<span>" + question['q']+ "</span>" + " ";
	}
	
	// <select> field
	if (num) { html += select(title, num); }
	else { html += select(title); }
	
	// <option> field
	options = question['options'];
	for (x in options) {
		html += option(options[x], options, question.translated);
	}
	
	html += "</select>";

	return html;
}

/* function dropdown (title, num) {
	var html = "";
	html += question['header']+" ";
	
	// <select> field
	if (num) { html += select(title, num); }
	else { html += select(title); }
	
	// <option> field
	options = question['options'];
	for (x in options) {
		html += option(options[x]);
	}
	
	html += "</select>"+" "+question['footer'];
	
	return html;
} */

function select(title, num) {

	// change value as appropriate
	switch (title) {
		case 'home': title = 'country0'; break;
		case 'homeyears': title = 'years0'; break;
		case 'current': title = 'country0'; break;
		case 'currentyears': title = 'years0'; break;
	}
	
	// hook up first field with blank() functionality
	if (title == "country" || title == "lang") {
		if (num > 0) {
			return "<select name=\'"+title+num+"\' onChange=\'blank("+num+","+multikey[title]+")\'>";
		}
	}
	
	// "normal" case
	if (title == "years" || title == "fluency") {
		return "<select name=\'"+title+num+"\'>";
	}
	
	// change questions with switching single country to multiple countries
	if (title == 'multinational') {
		return "<select name=\'"+title+"\' onChange='history(this.form."+title+".options[this.form."+title+".selectedIndex].value)'>";
	}
	return "<select name=\'"+title+"\'>";
}


function option(str, arr, translated) {

	if (translated)
	{
		for (key in arr) 
		{
			if (str == arr[key]) 
			{
				value = key;
			}
		}
	}
	else
	{
		value = str; 
	}	

	// switch names to values for certain cases
	switch(value) {
		case 'No':
		case 'male':
		//case ' ':
			value = 0; break;
		case 'Yes':
		case 'female':
		case 'at beginner\'s level':
			value = 1; break;
		case 'N/A':
		case 'well': 
			value = 2; break;
		case 'very well': 
			value = 3; break;
		case 'fluently':
			value = 4; break;
	}
	
	return "<option value=\""+value+"\">"+str+"</option>";
}

/*
function option(str) {
	value = str;
	// switch names to values for certain cases
	switch(str) {
		case 'have not':
		case 'male':
		//case ' ':
			value = 0; break;
		case 'have':
		case 'female':
		case 'at beginner\'s level':
			value = 1; break;
		case 'N/A':
		case 'well': 
			value = 2; break;
		case 'very well': 
			value = 3; break;
		case 'fluently':
			value = 4; break;
	}
	
	return "<option value=\""+value+"\">"+str+"</option>";
}
*/
var glo_country ;

function submit() {
	var response = $("#demo select");

	//var response = document.getElementById("demo").elements;
	glo_country = response[4].value;

//	console.log(response);

	for (var i = 0; i < response.length; i++) {
//		console.log($(response[i]).val());
		if ( ($(response[i]).val() == "" || $(response[i]).val() == " ") && $.inArray($(response[i]).attr("name"), required_fields) !== -1 ) {
			alert(<? echo _("'Please answer questions marked with an asterisk. This information will help us analyze the data we collect.'"); ?> );
			return;	
		}
	}


	/*
	for (var i = 0; i < required_fields.length; i++) {

		if (document.demo[required_fields[i]].options.selectedIndex == 0) {
			alert(<? echo _("'Please answer questions marked with an asterisk. This information will help us analyze the data we collect.'"); ?> );
			return;	
		}
	}
	*/

	glo_country = "Span";

	$$$("ajax_working").style.display = 'block';
	$$$("ajax_working").style.top = '-85px';
	$$$("demographics").style.opacity = '0.3';
	//$$$("navigate").style.opacity = '0.3'; 
	
	var data = {};
	for (var i = 0; i < response.length; i++) {	
		// creates a key-value pair to record in the data array
		data[$(response[i]).attr("name")] = $(response[i]).val();
		// creates a cookie with the same pair for future use
		if($("#cookies").is(":checked")) {
			createCookie($(response[i]).attr("name"), $(response[i]).val(), 1);
		}
	}
	data.numlangs = numlangs;
	data.numctries = numctries;
	data.participant_id = vars.participant_id;
	// console.log(data);

	/*data = {
		age: "19",
		country0: "Antarctica",
		country1: "China",
		education: "college",
		father: "China",
		gender: "0",
		lang0: "Chinese",
		mother: "China",
		multinational: "1",
		normalvision: "1",
		numctries: 1,
		numlangs: 0,
		profession: "Student",
		retake: "0",
		urban: "urban",
		web_usage: "10+",
		years0: "10",
		years1: "8"
	}*/

	// if the geolocation lookup service did not find a country, attempt to set vars.participant_country using country0
	if (vars.participant_country == "") vars.participant_country = data.country0;

	$.ajax({
    	type: 'POST',
    	url: 'includes/demographics.php',
    	data: data
    }).done( function (r) 
	{
		//data = $.parseJSON(data);
		$$$("next_button").style.display = "none"; 
		$$$("demographics").style.dispay = "none";
		$$$("ajax_working").style.display = "none";
		//Yan comment this out
		setTimeout(showCommentsPage(), 1000); 
	
		// setTimeout(finish(), 1000); 
	});
}
