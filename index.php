<!DOCTYPE html>
<html>
<head>
	<title><? echo _("Thinking Style Experiment"); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

	<!-- open graph tags -->
	<meta property="og:site_name" content="LabintheWild" />
	<meta property="og:title" content="Thinking Style Experiment" />
	<meta property="og:url" content="http://www.labinthewild.org/tmp12345/studies/analytic_test/" />
	<meta property="og:image" content="http://www.labinthewild.org/images/brain.png" />
	<meta property="og:description" content="Find out how your thinking style compares to others! " />

	<script>
		//var withTouch = <?php error_reporting(E_ALL ^ E_NOTICE); echo ($_GET["mobile"] == 'true') ? "true" : "false" ?>;
		var withTouch = ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch) ? true : false;
	</script>
	<script type="text/javascript" src="../common/js/jquery.js"></script>
	<script type="text/javascript" src="../common/js/zen.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
	<script src="../common/js/ga.js"></script>
	<script src="../../share/share.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/4.3.0/d3.min.js"></script>
    <script>
        $(function() {
            // For sharing on twitter it is good to have a short version of your URL; 
            // because it is not recorded among your open graph properties, it is good
            // to record it explicitly
            share.shorturl = "http://shar.es/1goMQI";
            share.linkedin = false,
            share.pinterest = false,
            share.sinaWeibo = true,
            // the makeButtons() method takes a CSS selector for DOM element(s) where the buttons
            // should be placed.  
            share.makeButtons(".sharing_buttons");
     //  		share.makeButtons("#end_share");// add social buttons at the bottom of result page
        });
    </script>
	<!--<script type="text/javascript">stLight.options({publisher: "ur-6dcdbcbb-3a30-96fb-6254-308d30c660ba"}); </script>-->
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../common/css/litw.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/zen.css" />
	<link rel="stylesheet" type="text/css" href="css/stim.css" />

	<?php
		include ("localization.php");
		$js_php_sources = array("js/demographics.js.php", "js/pages.js.php", "js/test.js.php", "js/helpers.js.php", "js/litw.js.php");
		require("../common/include/javascript.php");
	?>
	<!-- record locale string client-side -->
	<script>
		var litw_locale = <?php $echo_locale=true; include("localization.php"); ?>;
	</script>

	<style type="text/css">
		/* remove callout menu functionality */
		* { -webkit-touch-callout: none; }
		body { font-size: 20px; line-height: 26px; }
		h3 { margin-bottom: 5px; font-size: 30px; }
		#word_stims_box {
			margin-top: 50px;
		}
		.slide { position: relative; height: auto; }
		.smiley {
			margin:30px auto;
			display:block;
			width:200px;
			height:auto;
		}
		.stims { border: 5px solid transparent; }
		.inst_div { width: 600px; display: block; margin: 0 auto; text-align: left;}
		img.insts { display: block; margin: 0 auto; }
		ul li { padding: 10px 0px; }
		body { font-family: Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif; }

		/* style for result comment box*/
		#comments1 { text-align: left; }
		textarea.comments_box1 {
			-webkit-touch-callout: auto !important;
		    -webkit-user-select: auto !important;
		    -khtml-user-select: auto !important;
		    -moz-user-select: auto !important;
		    -ms-user-select: auto !important;
		    user-select: auto !important;
			padding: 15px;
			font-size: 14px;
			font-family: Optima, Arial, sans-serif;
			width: 560px;
			height: 70px;
			color: #999;
			display: block;
			/*margin: 0 auto;*/
		}

	</style>

</head>
<body>
<defs>
	<svg height="10" width="10" xmlns="http://www.w3.org/2000/svg" version="1.1"> <defs> <pattern id="circles-3" patternUnits="userSpaceOnUse" width="10" height="10"> <image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSd3aGl0ZScgLz4KICA8Y2lyY2xlIGN4PScyJyBjeT0nMicgcj0nMicgZmlsbD0nYmxhY2snLz4KPC9zdmc+" x="0" y="0" width="10" height="10"> </image> </pattern> </defs> </svg>
	<marker id="markerArrow" markerWidth="13" markerHeight="13" refX="2" refY="6"
			orient="auto">
		<path d="M2,2 L2,13 L8,7 L2,2" style="fill: #000000;" />
	</marker>
</defs>
	<div id="header" class="addthis_toolbox addthis_default_style " style="width: 900px; margin-left: auto; margin-right: auto;"></div>
	<div id="header_share" class="sharing_buttons"></div>
	<div id="content">
		<div id="studyinfo" class="slide"></div>
		<div id="demographics" class="slide"></div>
		<div id="trialNum" style="text-align:center;margin-top:10px;"></div>
		<div id="ia_content">
			<div id="headText" style="text-align:center;margin-top:20px;"></div>
			<div id="stimDiv" class="slide">
			    <img src="images/white.jpg" name="ia_left_question" id="ia_left_question" class="stims" /> 
			    <span>
			    	<img src="images/left_arrow.png" id="left_arrow" width="55px" height="55px" style="margin-bottom: 90px;" />
				    <img src="images/white.jpg" name="ia_prompt" id="ia_prompt" height="80px" width="auto" style="margin-bottom: 30px"/>
				    <img src="images/right_arrow.png" id="right_arrow" width="55px" height="55px" style="margin-bottom: 90px;" />
				</span>
			    <img src="images/white.jpg" name="ia_right_question" id="ia_right_question" class="stims" />
			</div>
			<div id="instPrac1" class="slide inst_div" style="margin-top:80px;"><span id="instPrac1text"></span></div>
			<div id="instPrac2" class="slide inst_div" style="margin-top:80px; width: 500px;"><span id="instPrac2text"></span></div>
			<div id="instPrac3" class="slide inst_div" style="margin-top:80px; with: 590px;"><span id="instPrac3text"></span></div>
			<div id="inst2" class="slide inst_div" style="margin-top: 80px;"><span id="inst2text"></span></div>
			<div id="warn" class="slide inst_div" style="margin-top:150px;"><span id="warning_text"></span></div>
			<div id="correct" class="slide" style="margin-top:100px;">
				<img src="images/success.png" class="smiley"/><span id="correct_text"></span>
			</div>
			<div id="incorrect" class="slide" style="margin-top:100px;">
				<img src="images/fail.png" class="smiley"/><span id="incorrect_text"></span>
			</div>
	   		<div id="empty" class="slide"></div>
		</div>

		<div id="word_stims_box" class="slide">
			<h4 id="word_stim_title"></h4>
			<br /><br />
			<table>
				<tr>
					<td id="left_word_stim" class="word_stims"></td>
					<td id="center_word_stim" class="word_stims"></td>
					<td id="right_word_stim" class="word_stims"></td>
				</tr>
				<tr>
					<td id="left_stim_caption" class="word_stim_caption"></td> 
					<td id="left_stim_caption" class="word_stim_caption"></td>
					<td id="left_stim_caption" class="word_stim_caption"></td>
				</tr>
			</table>
			<div id="continue"><button></button></div>
		</div>

		<div id="comments" class="slide"></div>
		<div id="results" class="slide">
			<div id="results_graph_text"></div>
			<div id="results_graph"></div>
<!--			<canvas id='results_canvas' width='800' height='200' style='margin-top:10px;'></canvas>-->
			<div id="results_share" class="sharing_buttons"></div>
		</div>
		<!-- <div id="end_share"></div> -->
		<div id="next_button"></div>
		<div id="ajax_working"></div>
		<div id="error"></div>
	</div>

</body>
</html>