<?php
/*************************************************************
 * localization.php
 *
 * Sets the locale based on the value stored in cookies and 
 * initiates gettext. Also checks and if necessary sets a 
 * unique sitewide ID and user country.
 *
 * Author: Trevor Croxson
 * Last modified: March 19, 2015
 *
 * © Copyright 2015 LabintheWild
 * For questions about this file and permission to use
 * the code, contact us at info@labinthewild.org
 *************************************************************/


$dictionary = array("en" => "en_US.UTF-8", "de" => "de_DE.UTF-8", "ja" => "ja_JP.UTF-8", "zh" => "zh_CN.UTF-8", "pt" => "pt_PT.UTF-8", "es" => "es_ES.UTF-8", "fr" => "fr_FR.UTF-8", "ru" => "ru_RU.UTF-8"); // <-- list mappings for all languages available here

if (!isset($_COOKIE["litw_locale"]) && !headers_sent()) setcookie("litw_locale", substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0 , 2), time() + 31536000, "/");
$lang = $_COOKIE["litw_locale"];

// if $lang is not set at this point, it means cookies aren't enabled, so default to browser language settings instead
if (!isset($lang)) $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0 , 2);

// if a locale parameter is supplied, this overrides everything else
if (isset($_GET["locale"])) $lang = $_GET["locale"];

if (isset($dictionary[$lang])) {
	$locale = $dictionary[$lang];
} else {
	$locale = "en_US.UTF-8";
	$lang = "en";
}

putenv("LC_ALL=".$locale);
setlocale(LC_ALL, $locale);
bindtextdomain("messages", dirname(__FILE__)."/locale");
textdomain("messages");
bind_textdomain_codeset("messages", 'UTF-8');
$locale = $lang;

if (isset($_POST["echo_locale"]) || isset($echo_locale)) echo json_encode($locale);

?>