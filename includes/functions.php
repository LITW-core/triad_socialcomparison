<?php 
/*************************************************************
 * functions.php
 * 
 * Contains common php functions that are used throughout the
 * experiment.
 * 
 * Author: Yuechen Zhao
 * Last Modified: July 21, 2012
 *
 * © Copyright 2014 LabintheWild
 * For questions about this file and permission to use
 * the code, contact us at info@labinthewild.org
 *************************************************************/

function __($str, $withQuote = true) {
	if ($withQuote)
    	echo '"'.gettext($str).'"'; 
    else
        echo gettext($str);
}

function logError($error, $participant_id) {
	// email to test administrator if an error occurred
	$to = "ADMIN_EMAIL";
	$to = ADMIN_EMAIL;
	$to = "tcroxson@umich.edu";
	$subject = "LITW - Aesthetics Test Encountered Error";
	$headers = "From: Lab in the Wild Bot<bot@labinthewild.org>";
	$body = 
"
Dear Test Administrator,

Participant No. $participant_id just encountered
a fatal error and was forced to drop out of the
Cultural Aesthetics Test on Lab in the Wild.

=== The Error ===

$error

Love,
Lab in the Wild Bot
";
	
	mail($to, $subject, $body, $headers);
}

?>