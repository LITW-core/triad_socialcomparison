﻿<?php
/*************************************************************
 * demographics.php
 * 
 * Submits the demographics information to the server and 
 * returns to the AJAX request the current participant_id and
 * country.
 * 
 * Author: Yuechen Zhao
 * Last Modified: June 15, 2016
 *
 * © Copyright 2016 LabintheWild
 * For questions about this file and permission to use
 * the code, contact us at info@labinthewild.org
 *************************************************************/
require_once("config.php");
error_reporting(E_ERROR);

(DEBUG_MODE ? $log = fopen("log.txt", "w") : false); 

$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
mysqli_set_charset($conn, "utf8");

$participant_id = mysqli_real_escape_string($conn, $_POST["participant_id"]);

// get answers to questions based on array of questions
$db = array("retake","age","gender","multinational","urban","father","mother","education","web_usage","profession","normalvision","lang0","country0","years0");
// $db = array("retake","age","gender","multinational","father","mother","education","normalvision","lang0","country0","years0");

	// check if items are submitted to change $db array
	$tmp = array(); 

	foreach ($db as $q) {
		if(isset($_POST[$q]))
		{
			$tmp[] = $q; 
		}
	}

	$db = $tmp; 

foreach ($db as $q) {
	$$q = mysqli_real_escape_string($conn, $_POST[$q]);
}

// Store paramters to bind later using call_user_func_array().
// This allows us to bind a variable number of parameters, such
// as additional languages and countries added by the user.
// The first element is a placeholder for the data type string,
// to be added by reference later.
$paramsToBind = array("");

// Hold variables for extra languages and countries.
$additionalQuestions = array();

// This string will accumulate the data type codes
// (e.g. 'ssisids') passed to bind_param(). We need to eventually
// be able to pass this string by reference (&$dataTypesToBind) to
// call_user_func_array().
$dataTypesToBind = "";

// We need to map field names to their data types when we're
// iterating through the $db array later.
$fieldDataTypes = array(
	"age" => "i",
	"country0" => "s",
	"years0" => "i",
	"lang0" => "s",
	"retake" => "i",
	"gender" => "i",
	"multinational" => "i",
	"user_agent" => "s",
	"father" => "s",
	"mother" => "s",
	"education" => "s",
	"normalvision" => "i",
	"urban" => "s",
	"web_usage" => "s",
	"profession" => "s",
	"participant_id" => "i"
);

$insert = "INSERT INTO demographics (`participant_id`, ";
$values = "VALUES (?, ";
$dataTypesToBind .= $fieldDataTypes["participant_id"];
// Pass parameters to bind by reference. This is a requirement of call_user_func_array().
$paramsToBind[] = &$participant_id;

// additional languages
$numlangs = $_POST["numlangs"];
for ($i = 1; $i <= $numlangs; $i++) {
	$insert .= "lang".$i.", "."fluency".$i.",";
	$values .= "?, ?, ";
	$dataTypesToBind .= "si";
	$additionalQuestions["lang".$i] = mysqli_real_escape_string($conn, $_POST["lang".$i]);
	$additionalQuestions["fluency".$i] = mysqli_real_escape_string($conn, $_POST["fluency".$i]);
	$paramsToBind[] = &$additionalQuestions["lang".$i];
	$paramsToBind[] = &$additionalQuestions["fluency".$i];
}

// additional countries
$numctries = $_POST["numctries"];
for ($i = 1; $i <= $numctries; $i++) {
	$insert .= "country".$i.", "."years".$i.",";
	$values .= "?, ?, ";
	$dataTypesToBind .= "sd";
	$additionalQuestions["country".$i] = mysqli_real_escape_string($conn, $_POST["country".$i]);
	$additionalQuestions["years".$i] = mysqli_real_escape_string($conn, $_POST["years".$i]);
	$paramsToBind[] = &$additionalQuestions["country".$i];
	$paramsToBind[] = &$additionalQuestions["years".$i];
}

// basic questions
$questions = count($db);
for ($i = 0; $i < $questions; $i++) {
	$insert .= "`" . $db[$i] . "`";
	$insert .= ($i < $questions - 1)? ", ": ") ";
	$values .= "?";
	$values .= ($i < $questions - 1)? ", ": ") ";
	$dataTypesToBind .= $fieldDataTypes[$db[$i]];
	$paramsToBind[] = &$$db[$i];
}

// Add data type string.
$paramsToBind[0] = &$dataTypesToBind;

$query = $insert . $values;
$stmt = $conn->prepare($query);

if ( false===$stmt ) {
	die('prepare() failed: ' . htmlspecialchars($conn->error));
}

$rc = call_user_func_array(array($stmt, 'bind_param'), $paramsToBind);

if ( false===$rc ) {
	// again execute() is useless if you can't bind the parameters. Bail out somehow.
	die('bind_param() failed: ' . htmlspecialchars($stmt->error));
}

$rc = $stmt->execute();
	
// catch errors
if (!$rc) {
	if (DEBUG_MODE)
	{
		fwrite($log, "Case 1\n");
		fwrite($log, mysqli_error($conn)); 
		fclose($log); 			
	}
	echo "Failed:" . mysqli_error($conn);
	//logError(("Failed: Cannot record demographics! MySQL said: " . mysql_error()), -1);
	die();
} else {

}

?>