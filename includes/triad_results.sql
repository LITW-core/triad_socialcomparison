-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: mysql.labinthewild.org
-- Generation Time: Jun 08, 2016 at 01:08 PM
-- Server version: 5.6.25-log
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `triad_results`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `participant_id` int(11) NOT NULL COMMENT 'The participant ID, which is linked to the participant_id field in the demographics table',
  `general` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'General comments about the test',
  `cheating` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Whether they cheated, and if so, how',
  `technical` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Whether they had technical difficulties, and if so, the problem.',
  `page_version` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'version of the results page seen. 2 = global comparison version; 1 = country comparison version; 0 = no comparison version'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments1`
--

CREATE TABLE `comments1` (
  `comment2` text COLLATE utf8_unicode_ci NOT NULL,
  `participant_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `demographics`
--

CREATE TABLE `demographics` (
  `participant_id` int(8) NOT NULL,
  `retake` tinyint(1) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `multinational` tinyint(1) NOT NULL,
  `father` varchar(100) CHARACTER SET utf8 NOT NULL,
  `mother` varchar(100) CHARACTER SET utf8 NOT NULL,
  `education` varchar(100) CHARACTER SET utf8 NOT NULL,
  `normalvision` tinyint(1) NOT NULL,
  `country0` varchar(100) CHARACTER SET utf8 NOT NULL,
  `years0` int(2) NOT NULL,
  `country1` varchar(100) CHARACTER SET utf8 NOT NULL,
  `country2` varchar(100) CHARACTER SET utf8 NOT NULL,
  `country3` varchar(100) CHARACTER SET utf8 NOT NULL,
  `country4` varchar(100) CHARACTER SET utf8 NOT NULL,
  `country5` varchar(100) CHARACTER SET utf8 NOT NULL,
  `years1` decimal(2,0) NOT NULL,
  `years2` decimal(2,0) NOT NULL,
  `years3` decimal(2,0) NOT NULL,
  `years4` decimal(2,0) NOT NULL,
  `years5` decimal(2,0) NOT NULL,
  `lang0` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lang1` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lang2` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lang3` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lang4` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lang5` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lang6` varchar(100) CHARACTER SET utf8 NOT NULL,
  `fluency1` int(1) NOT NULL,
  `fluency2` int(1) NOT NULL,
  `fluency3` int(1) NOT NULL,
  `fluency4` int(1) NOT NULL,
  `fluency5` int(1) NOT NULL,
  `fluency6` int(1) NOT NULL,
  `age` int(3) NOT NULL,
  `urban` varchar(10) CHARACTER SET utf8 NOT NULL,
  `web_usage` varchar(5) CHARACTER SET utf8 NOT NULL,
  `profession` varchar(40) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ip_country`
--

CREATE TABLE `ip_country` (
  `participant_id` int(8) NOT NULL,
  `user_agent` varchar(200) CHARACTER SET latin1 NOT NULL COMMENT 'browser user agent info',
  `ip_country` varchar(100) CHARACTER SET latin1 NOT NULL COMMENT 'IP-generated country',
  `current_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'current time on page load',
  `content_language` varchar(5) CHARACTER SET latin1 NOT NULL COMMENT 'display language of experiment content',
  `ip_city` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `results_page_comments`
--

CREATE TABLE `results_page_comments` (
  `participant_id` int(11) NOT NULL,
  `results_page_comments` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `results_page_metrics`
--

CREATE TABLE `results_page_metrics` (
  `index` int(11) NOT NULL,
  `participant_id` int(8) NOT NULL,
  `entertime` bigint(20) NOT NULL COMMENT 'timestamp when participant was shown the results page',
  `height` int(7) NOT NULL COMMENT 'height of the results page viewport, in pixels',
  `width` int(7) NOT NULL COMMENT 'width of the results page viewport, in pixels',
  `isscroll` tinyint(1) NOT NULL COMMENT 'whether the user scrolled the results page. 0 = didn''t scroll; 1 = scrolled',
  `maxscroll` float NOT NULL COMMENT 'the furthest the user scrolled the results page, as a percentage of the document height',
  `leavetime` bigint(20) NOT NULL COMMENT 'timestamp for when the user closed the results page window',
  `page_version` tinyint(1) NOT NULL COMMENT 'version of the results page seen. 2 = global comparison version; 1 = country comparison version; 0 = no comparison version',
  `graph_hover` bigint(20) NOT NULL COMMENT 'time spent hovering over results page graphs'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `share_click`
--

CREATE TABLE `share_click` (
  `index` int(11) NOT NULL,
  `participant_id` int(11) NOT NULL,
  `service` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'the social media service clicked on',
  `location` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'the icon set clicked on (in header or body)',
  `click_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'the time of the click'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trials`
--

CREATE TABLE `trials` (
  `index` int(11) NOT NULL,
  `participant_id` int(11) NOT NULL COMMENT 'unique participant id',
  `trial_type` varchar(15) NOT NULL COMMENT 'can be "triad", "ia_test", or "ia_instPrac2" (the ia instructions screen)',
  `overall_trial_num` tinyint(4) NOT NULL COMMENT 'the cumulative trial number, increasing through the entire experiment',
  `triad_left_stim` varchar(30) NOT NULL COMMENT 'the leftmost word seen',
  `triad_center_stim` varchar(30) NOT NULL COMMENT 'the center word seen',
  `triad_right_stim` varchar(30) NOT NULL COMMENT 'the rightmost word seen',
  `triad_response_1` varchar(30) NOT NULL COMMENT 'participant''s first selection',
  `triad_response_2` varchar(30) NOT NULL COMMENT 'participant''s second selection',
  `triad_analysis` varchar(30) NOT NULL COMMENT 'can be "analytic", "holistic", or "neither"',
  `rt` int(20) NOT NULL COMMENT 'the overall response time for this trial, in milliseconds',
  `ia_response` tinyint(11) NOT NULL COMMENT 'the participant''s chosen stim. Can be 1 (for the left stim) or 2 (for the right stim)',
  `ia_left_stim` varchar(50) NOT NULL COMMENT 'image filename for the left stim',
  `ia_right_stim` varchar(50) NOT NULL COMMENT 'image filename for the right stim',
  `ia_prompt` varchar(50) NOT NULL COMMENT 'image filename for the prompt stim',
  `ia_analysis` varchar(20) NOT NULL COMMENT 'can be "analytic" or "holistic"',
  `overall_score` tinyint(6) NOT NULL COMMENT 'user''s final computed score. Ranges from -24 (most analytic) to +24 (most holistic)',
  `phase` tinyint(4) NOT NULL COMMENT 'the experiment phase. Can be 1, 2, or 3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`participant_id`);

--
-- Indexes for table `demographics`
--
ALTER TABLE `demographics`
  ADD PRIMARY KEY (`participant_id`);

--
-- Indexes for table `ip_country`
--
ALTER TABLE `ip_country`
  ADD PRIMARY KEY (`participant_id`);

--
-- Indexes for table `results_page_comments`
--
ALTER TABLE `results_page_comments`
  ADD PRIMARY KEY (`participant_id`);

--
-- Indexes for table `results_page_metrics`
--
ALTER TABLE `results_page_metrics`
  ADD PRIMARY KEY (`index`);

--
-- Indexes for table `share_click`
--
ALTER TABLE `share_click`
  ADD PRIMARY KEY (`index`);

--
-- Indexes for table `trials`
--
ALTER TABLE `trials`
  ADD PRIMARY KEY (`index`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ip_country`
--
ALTER TABLE `ip_country`
  MODIFY `participant_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33042;
--
-- AUTO_INCREMENT for table `results_page_metrics`
--
ALTER TABLE `results_page_metrics`
  MODIFY `index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13348;
--
-- AUTO_INCREMENT for table `share_click`
--
ALTER TABLE `share_click`
  MODIFY `index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=328;
--
-- AUTO_INCREMENT for table `trials`
--
ALTER TABLE `trials`
  MODIFY `index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=742495;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
