<?php
/*************************************************************
 * data.php
 * 
 * Submits the experimental data to the server. Submits the
 * data recorded for every trial of the experiment.
 * 
 * Author: Trevor Croxson
 * Based on code by Rishav Mukherji
 * Last Modified: February 3, 2015
 *
 * © Copyright 2015 LabintheWild
 * For questions about this file and permission to use
 * the code, contact us at info@labinthewild.org
 *************************************************************/

require_once("config.php");

$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);


$participant_id = $_POST["participant_id"];
$ia_results = json_decode(stripslashes($_POST["ia_results"]), $assoc=true);
$triad_results = json_decode(stripslashes($_POST["triad_results"]), $assoc=true);
$analytic_score = $_POST["analytic_score"];
$holistic_score = $_POST["holistic_score"];

foreach ($ia_results as $trial) {
	$ia_trial_type = $trial["type"];
	$phase = $trial["phase"];
	$overall_trial_num = $trial["overall_trial_num"];
	$ia_response = $trial["response"];
	$ia_left_stim = $trial["left_question"];
	$ia_right_stim = $trial["right_question"];
	$ia_prompt = $trial["prompt"];
	$ia_analysis = $trial["analysis"];
	$rt = $trial["rt"];
	$trial_type = $trial["type"];

	$stmt = $conn->prepare("INSERT INTO trials (`participant_id`, `trial_type`, `phase`, `overall_trial_num`, `ia_response`, `ia_left_stim`, `ia_right_stim`, `ia_prompt`, `ia_analysis`, `analytic_score`, `holistic_score`, `rt`) 
													VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	
	if ( false===$stmt ) {
		error_log('prepare() failed: ' . htmlspecialchars($conn->error));
	}
	
	$rc = $stmt->bind_param('isiiissssiii', $participant_id, $trial_type, $phase, $overall_trial_num, $ia_response, $ia_left_stim, $ia_right_stim, $ia_prompt, $ia_analysis, $analytic_score, $holistic_score, $rt);
	
	if ( false===$rc ) {
		// again execute() is useless if you can't bind the parameters. Bail out somehow.
		error_log('bind_param() failed: ' . htmlspecialchars($stmt->error));
	}
	
	$rc = $stmt->execute();
	// execute() can fail for various reasons. And may it be as stupid as someone tripping over the network cable
	// 2006 "server gone away" is always an option
	if ( false===$rc ) {
		echo "Failed: " . mysqli_error($conn);
		error_log(("Failed: " . mysqli_error($conn)), $participant_id);
		error_log('execute() failed: ' . htmlspecialchars($stmt->error));
	}
	
	$stmt->close();
}

foreach ($triad_results as $trial) {
	$overall_trial_num = $trial["overall_trial_num"];
	$phase = $trial["phase"];
	$triad_left_word_stim = $trial["left_word_stim"];
	$triad_center_word_stim = $trial["center_word_stim"];
	$triad_right_word_stim = $trial["right_word_stim"];
	$triad_analysis = $trial["analysis"];
	$triad_rt = $trial["rt"];
	$triad_response_1 = $trial["response_1"];
	$triad_response_2 = $trial["response_2"];
	$trial_type = $trial["type"];
	$rt = $trial["rt"];

	$stmt = $conn->prepare("INSERT INTO trials (`participant_id`, `trial_type`, `phase`, `overall_trial_num`, `analytic_score`, `holistic_score`, `triad_left_stim`, `triad_center_stim`, `triad_right_stim`, `triad_response_1`, `triad_response_2`, `triad_analysis`, `rt`) 
					 								VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	
	if ( false===$stmt ) {
		error_log('prepare() failed: ' . htmlspecialchars($conn->error));
	}
	
	$rc = $stmt->bind_param('isiiiissssssi', $participant_id, $trial_type, $phase, $overall_trial_num, $analytic_score, $holistic_score, $triad_left_word_stim, $triad_center_word_stim, $triad_right_word_stim, $triad_response_1, $triad_response_2, $triad_analysis, $rt );
	
	if ( false===$rc ) {
		// again execute() is useless if you can't bind the parameters. Bail out somehow.
		error_log('bind_param() failed: ' . htmlspecialchars($stmt->error));
	}
	
	$rc = $stmt->execute();
	// execute() can fail for various reasons. And may it be as stupid as someone tripping over the network cable
	// 2006 "server gone away" is always an option
	if ( false===$rc ) {
		echo "Failed: " . mysqli_error($conn);
		error_log(("Failed: " . mysqli_error($conn)), $participant_id);
		error_log('execute() failed: ' . htmlspecialchars($stmt->error));
	}
	
	$stmt->close();
}

if (!$rc) {
	echo "Failed: " . mysqli_error($conn);
	error_log(("Failed: " . mysqli_error($conn)), $participant_id);
}

?>
