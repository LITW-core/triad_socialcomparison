<?php

require_once("config.php");
require_once("functions.php");

$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
mysqli_set_charset($conn, "utf8");

$participant_id = mysqli_real_escape_string($conn, $_POST["participant_id"]);
$service = mysqli_real_escape_string($conn, $_POST["service"]);
$location = mysqli_real_escape_string($conn, $_POST["location"]);

$stmt = $conn->prepare("INSERT INTO share_click (`participant_id`, `service`, `location`)
												VALUES (?, ?, ?)");

if ( false===$stmt ) {
	die('prepare() failed: ' . htmlspecialchars($conn->error));
}

$rc = $stmt->bind_param('iss', $participant_id, $service, $location);

if ( false===$rc ) {
	// again execute() is useless if you can't bind the parameters. Bail out somehow.
	die('bind_param() failed: ' . htmlspecialchars($stmt->error));
}

$rc = $stmt->execute();
// execute() can fail for various reasons. And may it be as stupid as someone tripping over the network cable
// 2006 "server gone away" is always an option
if ( false===$rc ) {
	die('execute() failed: ' . htmlspecialchars($stmt->error));
}

$stmt->close();

?>
