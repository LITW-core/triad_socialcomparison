<?php
/*************************************************************
 * recordCountry.php
 *
 * Records the country name based on IP address, as well as
 * other info
 *
 * Author: Trevor Croxson
 * Last modified: January, 2015
 *
 * © Copyright 2015 Lab in the Wild
 * For questions about this file and permission to use
 * the code, contact us at info@labinthewild.org
 *************************************************************/
require ("config.php");
include ("functions.php");

require_once("../../../vendor/autoload.php");
use GeoIp2\WebService\Client;

$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
mysqli_set_charset($conn, "utf8");

$country = "";
$city = "";
$locale = $_POST["locale"];
//print_r($locale);

$user_agent = mysqli_real_escape_string($conn, $_SERVER["HTTP_USER_AGENT"]);
$ip = mysqli_real_escape_string($conn, $_SERVER["REMOTE_ADDR"]);

// MaxMind will crash if fed localhost ip address
if ($ip != "::1" && $ip != "127.0.0.1") {
	// grab city and country info from the MaxMind service
	$client = new Client(98319, "tLQRtRr2VPEH");
	$record = $client->city($ip);
	$country = $record->country->name;
	$city = $record->city->name;
	$country = mysqli_real_escape_string($conn, $country);
	$city = mysqli_real_escape_string($conn, $city);
}

$stmt = $conn->prepare("INSERT INTO ip_country (`ip_country`, `ip_city`, `user_agent`, `content_language`)
											  VALUES (?, ?, ?, ?)");

if ( false===$stmt ) {
	die('prepare() failed: ' . htmlspecialchars($conn->error));
}

$rc = $stmt->bind_param('ssss', $country, $city, $user_agent, $locale);

if ( false===$rc ) {
	// again execute() is useless if you can't bind the parameters. Bail out somehow.
	die('bind_param() failed: ' . htmlspecialchars($stmt->error));
}

$rc = $stmt->execute();
// execute() can fail for various reasons. And may it be as stupid as someone tripping over the network cable
// 2006 "server gone away" is always an option
if ( false===$rc ) {
	die('execute() failed: ' . htmlspecialchars($stmt->error));
}

$stmt->close();

$participant_id = mysqli_insert_id($conn);

//return participant id, browser language, and country to js:
$results = array("participant_id" => $participant_id, "locale" => $locale, "ip_country" => $country);
echo json_encode($results);

?>