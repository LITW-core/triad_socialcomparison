<?php
/*************************************************************
 * comments.php
 * 
 * Submits the comments that a participant makes to the server.
 * Includes general comments, comments about how they cheated,
 * and technical difficulties.
 * 
 * Author: Yuechen Zhao
 * Last Modified: July 21, 2012
 *
 * Â© Copyright 2014 LabintheWild
 * For questions about this file and permission to use
 * the code, contact us at info@labinthewild.org
 *************************************************************/

require_once("config.php");
require_once("functions.php");

// insert comments into database
$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
mysqli_set_charset($conn, "utf8");

$general = mysqli_real_escape_string($conn, $_POST["general"]);
$cheating = mysqli_real_escape_string($conn, $_POST["cheating"]);
$technical = mysqli_real_escape_string($conn, $_POST["technical"]);
$participant_id = mysqli_real_escape_string($conn, $_POST["participant_id"]);
$page_version = mysqli_real_escape_string($conn, $_POST["index"]);

$query = "INSERT INTO comments (`participant_id`, `general`, `cheating`, `technical`) VALUES ('$participant_id', '$general', '$cheating', '$technical')";

$result = mysqli_query($conn, $query);
if (!$result)
	logError(("Failed: Database error recording participants comments about test. MySQL said: " . mysqli_error($conn)), $participant_id);

// email comments out if participant made comments
if ($general != "" || $technical != "") {
	$to = ADMIN_EMAIL;
	$subject = "LITW - Analytic Test Comment";
	$headers = "From: Lab in the Wild Bot<bot@labinthewild.org>";
	$body = 
"
Dear Test Administrator,

Participant No. $participant_id just made some 
general and/or technical comments about the 
Analytic Test on Lab in the Wild.

=== Technical Comment ===

$technical

=== General Comment ===

$general

Love,
Lab in the Wild Bot
";

	mail($to, $subject, $body, $headers);
}

?>