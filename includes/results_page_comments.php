<?php
/*************************************************************
 * results_page_comments.php
 *
 * Author: Yuechen Zhao
 * Last Modified: July 21, 2012
 *
 * © Copyright 2014 LabintheWild
 * For questions about this file and permission to use
 * the code, contact us at info@labinthewild.org
 *************************************************************/

require_once("config.php");
require_once("functions.php");

// insert comments into database
$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
mysqli_set_charset($conn, "utf8");

$results_page_comments = mysqli_real_escape_string($conn, $_POST["results_page_comments"]);
$participant_id = mysqli_real_escape_string($conn, $_POST["participant_id"]);

$stmt = $conn->prepare("INSERT INTO results_page_comments (`participant_id`, `results_page_comments`) 
												VALUES (?, ?)");

if ( false===$stmt ) {
	die('prepare() failed: ' . htmlspecialchars($conn->error));
}

$rc = $stmt->bind_param('is', $participant_id, $results_page_comments);

if ( false===$rc ) {
	// again execute() is useless if you can't bind the parameters. Bail out somehow.
	die('bind_param() failed: ' . htmlspecialchars($stmt->error));
}

$rc = $stmt->execute();
// execute() can fail for various reasons. And may it be as stupid as someone tripping over the network cable
// 2006 "server gone away" is always an option
if ( false===$rc ) {
	die('execute() failed: ' . htmlspecialchars($stmt->error));
}

$stmt->close();

?>