<?php
/*************************************************************
 * results_page_metrics.php
 * 
 * Author: Yuechen Zhao
 * Last Modified: June 15, 2016
 *
 * © Copyright 2016 LabintheWild
 * For questions about this file and permission to use
 * the code, contact us at info@labinthewild.org
 *************************************************************/

require_once("config.php");
require_once("functions.php");

$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
mysqli_set_charset($conn, "utf8");

$participant_id = mysqli_real_escape_string($conn, $_POST["participant_id"]);
$participant_country = mysqli_real_escape_string($conn, $_POST["participant_country"]);
$browser_lang = mysqli_real_escape_string($conn, $_POST["browser_lang"]);
$condition = mysqli_real_escape_string($conn, $_POST["condition"]);
$holistic_score = mysqli_real_escape_string($conn, $_POST["holistic_score"]);
$analytic_score = mysqli_real_escape_string($conn, $_POST["analytic_score"]);
$read_more_click = mysqli_real_escape_string($conn, $_POST["read_more_click"]);
$show_less_click = mysqli_real_escape_string($conn, $_POST["show_less_click"]);
$resultsExpHover = mysqli_real_escape_string($conn, $_POST["resultsExpHover"]);
$leavetime = mysqli_real_escape_string($conn, $_POST["leavetime"]);
$entertime = mysqli_real_escape_string($conn, $_POST["entertime"]);
$height = mysqli_real_escape_string($conn, $_POST["height"]);
$width = mysqli_real_escape_string($conn, $_POST["width"]);
$isscroll = mysqli_real_escape_string($conn, $_POST["isscroll"]);
$maxscroll = mysqli_real_escape_string($conn, $_POST["maxscroll"]);
//$page_version = mysqli_real_escape_string($conn, $_POST["index"]);
$graphHover = mysqli_real_escape_string($conn, $_POST["graphHover"]);

$stmt = $conn->prepare("INSERT INTO results_page_metrics (`participant_id`, `participant_country`, `browser_lang`, `condition`, `holistic_score`, `analytic_score`, `read_more_click`, `show_less_click`, `resultsExpHover`, `entertime`, `height`, `width`, `leavetime`, `isscroll`, `maxscroll`, `graph_hover`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

if ( false===$stmt ) {
	error_log('prepare() failed: ' . htmlspecialchars($conn->error));
}

$rc = $stmt->bind_param('issiiiiiiiiiiidi', $participant_id, $participant_country,$browser_lang, $condition, $holistic_score, $analytic_score, $read_more_click, $show_less_click, $resultsExpHover, $entertime, $height, $width, $leavetime, $isscroll, $maxscroll, $graphHover);

if ( false===$rc ) {
	// again execute() is useless if you can't bind the parameters. Bail out somehow.
	error_log('bind_param() failed: ' . htmlspecialchars($stmt->error));
}

$rc = $stmt->execute();
// execute() can fail for various reasons. And may it be as stupid as someone tripping over the network cable
// 2006 "server gone away" is always an option
if ( false===$rc ) {
	error_log('execute() failed: ' . htmlspecialchars($stmt->error));
}

$stmt->close();

?>
